<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>

<div class="container">

	<div class="main-content">

		<ol class="breadcrumb">
		    <li><a href="#">Blum Products</a></li>
		    <li><a href="#">Perfecting Motion</a></li>
		    <li><a href="#">Blum AMBIA-LINE</a></li>
		    <li><a href="#">AMBIA-LINE orion grey</a></li>
		    <li class="active">AMBIA-LINE series 5 Blum AMBIA-LINE Steel Frame Series 5 Orion Grey Size: 270mm ZC7F300R</li>
		</ol>

		<a href="#" class="go-back hidden-xs">Back</a>

		<!-- Product header -->
		<div class="product-header clearfix">
			<div class="col-md-2 product-keycode">
				<h1>WB8043</h1>
			</div>
			<div class="col-md-10 product-title">
				<h1>Blum TANDEMBOX Intivo D Height 211MM drawer BOXCOVER- integrated BLUMOTION 500MM in Stainless Steel 65KG</h1>
			</div>
		</div>

		<div class="product-body product-kit-body clearfix">

			<?php
				$kit_parts = array(
					array(
					'image' => '358L500IA_1.jpg',
					'name' => 'Blum TANDEMBOX Intivo L Sides. Size: 500mm In Stainless Steel Anti fingermark 378L5002IA',
					'price' => '85.5208',
					'ID' => '1',
					),
					array(
					'image' => '556.4501B_1.jpg',
					'name' => 'Blum TANDEMBOX Carcase Runner Set 65Kg. Size: 500mm, With Integrated BLUMOTION 556.5001B',
					'price' => '47.4880',
					'ID' => '2',
					),
					array(
					'image' => 'Z30DSLN_1.jpg',
					'name' => 'Blum TANDEMBOX Intivo Back Fixings Bracket, D Height, Left And Right In Nickel Z30D000Sl',
					'price' => '11.3178',
					'ID' => '3',
					),
					array(
					'image' => 'Z36L2GIA_1.jpg',
					'name' => 'Blum TANDEMBOX Intivo Front Fixing Bracket, EXPANDO ZSF.532E',
					'price' => '2.9869',
					'ID' => '4',
					),
					array(
					'image' => 'Z37G440DL_2.jpg',
					'name' => 'Blum TANDEMBOX Intivo BOXCOVER front & back caps. Stainless Steel Anti fingermark Z36L002G.I',
					'price' => '22.5568',
					'ID' => '5',
					),
					array(
					'image' => 'Z37G440DR_2.jpg',
					'name' => 'Blum TANDEMBOX Intivo BOXCOVER Satin Glass Left Side. Size: 500mm Z37G440D.IN',
					'price' => '34.0126',
					'ID' => '6',
					),
					array(
					'image' => 'ZSF532E_1.jpg',
					'name' => 'Blum TANDEMBOX Intivo BOXCOVER Satin Glass Right Side. Size: 500mm Z37G440D.IN',
					'price' => '34.0130',
					'ID' => '7',
					),
				);
			?>

			<div class="col-xs-12 col-sm-12 col-md-7">
				<div class="row">

					<div class="col-xs-4 col-sm-4 col-md-2 image-slider clearfix hidden-xs">

						<button class="slider-btn slider-prev"></button>

						<div class="product-slider-container">

							<?php foreach($kit_parts as $part) { ?>
							<div class="galImg">
								<a href="javascript:void(0);"><img src="img/product-kits/parts/<?php echo $part['image']; ?>"  class="img-responsive" /></a>
							</div>
							<?php } ?>
						</div>
						<button class="slider-btn slider-next"></button>

					</div>


					<!-- Main featured image-->
					<div class="col-xs-12 col-sm-8 col-md-8 feature-img">

						<a href="img/product-kits/WB8043_1.jpg" class="image-zoom-btn" data-toggle="lightbox"><img src="img/icons/image-zoom.png"/></a>


						<a class="lightbox" href="img/product-kits/WB8043_1.jpg" data-toggle="lightbox">
						   <img src="img/product-kits/WB8043_1.jpg" class="img-responsive" />
						</a>

					</div>



					<div class="col-xs-12 clearfix thumb-slider hidden-lg hidden-md hidden-sm">
						<button class="slider-btn thumb-slider-prev col-xs-1"></button>

						<div class="product-slider-container col-xs-10">

							<?php foreach($kit_parts as $part) { ?>
							<div class="galImg col-xs-4">
								<a href="javascript:void(0);"><img src="img/product-kits/parts/<?php echo $part['image']; ?>"   class="img-responsive" /></a>
							</div>
							<?php } ?>
						</div>

						<button class="slider-btn thumb-slider-next col-xs-1"></button>

					</div>













					<!-- Product Kit Info -->
					<div class="product-kit-options col-xs-12 col-sm-12 col-md-offset-2 col-md-10">

						<div class="col-md-5"><span class="sync-set">Synchronisation Set</span></div>
						<div class="col-md-7 sync-text"><p>Syncronisation sets are required for drawers with an internal cabinet width of 550mm and above synchronisation set</p></div>

						<div class="col-md-12">

							<div class="radio">
								<label>
									<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
									As a single kit - recommended
								</label>
							</div>

							<div class="radio">
								<label>
									<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
									As individual items - if you need to alter each product qty in cart
								</label>
							</div>

						</div>

					</div>

				</div>
			</div>

			<!-- Product details, quantity, price -->
			<div class="col-xs-12 col-sm-12 col-md-5 product-details">

				<div class="product-kit-list clearfix">

					<div class="clearfix product-list-header hidden-xs">
			          <div class="col-sm-3 col-md-1">&nbsp;</div>
			          <div class="col-sm-5 col-md-6">Product name</div>
			          <div class="col-sm-2 col-md-1">Qty</div>
			          <div class="col-sm-2 col-md-3">Unit price</div>
			        </div>

			        <?php foreach($kit_parts as $part) { ?>
					<div class="clearfix">
						<div class="clearfix product-list-row">

		            <div class="col-xs-4 col-sm-3 col-md-2 product-image"><img src="img/product-kits/parts/<?php echo $part['image']; ?>" class="img-responsive" /></div>
		            <div class="col-xs-8 col-sm-5 col-md-6 product-name">
		            	<span class="product-namekey"><?php echo $part['name']; ?></span>
		            	<a alt="<?php echo $part['ID']; ?>" class="hidden-xs">show / hide kit item</a>
		            	<!-- <p><?php echo $part['name']; ?> description</p> -->
		            </div>
		            <div class="col-xs-6 col-sm-2 col-md-1 product-qty"><span class="visible-xs-block">Qty: </span><input type="number" id="quantity_input" value="1" class="text-center" readonly/></div>
		            <div class="col-xs-6 col-sm-2 col-md-3 product-price"><span class="visible-xs-block">Price: </span>$<?php echo $part['price']; ?></div>

	          </div>

			          <div class="info-option option-<?php echo $part['ID']; ?> clearfix hidden-xs">

				          <div class="hidden-lg hidden-md hidden-sm col-xs-3"></div>
				            <div class="col-xs-6 col-sm-3 col-md-2 product-image">
					            <a href="#"></a>
					            <div class="radio">
									<label>
										<input type="radio" name="optionsRadios-<?php echo $part['ID']; ?>" id="optionsRadios1" value="option1">
										<img src="img/product-kits/parts/<?php echo $part['image']; ?>" class="img-responsive" />
									</label>
								</div>
					        </div>
				            <div class="col-xs-12 col-sm-5 col-md-6 product-name">
				            	<span class="product-namekey"><?php echo $part['name']; ?></span>
				            	<!-- <p><?php echo $part['name']; ?> description</p> -->
				            </div>
				            <div class="col-xs-6 col-sm-2 col-md-1 product-qty"><span class="visible-xs-block">Qty: </span><input type="number" id="quantity_input" class="text-center" value="1" /></div>
				            <div class="col-xs-6 col-sm-2 col-md-3 product-price"><span class="visible-xs-block">Price: </span>$<?php echo $part['price']; ?></div>


			          </div>

			          <div class="info-option option-<?php echo $part['ID']; ?> clearfix hidden-xs">
				        <div class="hidden-lg hidden-md hidden-sm col-xs-3"></div>
			            <div class="col-xs-6 col-sm-3 col-md-2 product-image">

				            <div class="radio">
								<label>
									<input type="radio" name="optionsRadios-<?php echo $part['ID']; ?>" id="optionsRadios1" value="option1">
									<img src="img/product-kits/parts/<?php echo $part['image']; ?>" class="img-responsive" />
								</label>
							</div>
				        </div>
			            <div class="col-xs-12 col-sm-5 col-md-6 product-name">
			            	<span class="product-namekey">Blum TANDEMBOX Intivo Back Fixings Bracket, D Height, Left And Right In Nickel Z30D000Sl</span>
			            	<!-- <p><?php echo $part['name']; ?> description</p> -->
			            </div>
			            <div class="col-xs-6 col-sm-2 col-md-1 product-qty"><span class="visible-xs-block">Qty: </span><input type="number" id="quantity_input" class="text-center" value="1" /></div>
			            <div class="col-xs-6 col-sm-2 col-md-3 product-price"><span class="visible-xs-block">Price: </span>$250</div>
			          </div>

					</div>


			        <?php } ?>

				</div>

				<div class="qty-price clearfix">

					<div class="col-xs-6 col-sm-6 col-md-6 quantity">
						<label for="quantity_input">Qty</label>
						<input type="number" id="quantity_input" value="1" />
					</div>

					<div class="col-xs-6 col-sm-6 col-md-6 price">
						Price (ex GST): <span class="bold">$240.8828</span>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-12">
						<p>Products available for individual purchase unless otherwise stated.</p>
					</div>
				</div>

				<div class="product-buttons clearfix">

					<div class="col-xs-12 col-sm-6 col-md-6 text-center add-to-favs">
						<a href="#">Add to favourites</a>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-6 add-to-cart">
						<a href="step10.php">Add to cart</a>
					</div>
				</div>

			</div>

		</div>

		<div class="product-information product-kit-information">

			<!-- Information Tabs -->
			<div class="hidden-sm hidden-xs">
				<ul class="nav nav-tabs product-info-tabs" role="tablist">
				<li role="presentation" class="col-md-5ths active">
					<a href="#home" aria-controls="home" role="tab" data-toggle="tab">Description</a>
				</li>
				<li role="presentation" class="col-md-5ths">
					<a href="#installation-videos" aria-controls="installation-videos" role="tab" data-toggle="tab">Installation Videos</a>
				</li>
				<li role="presentation" class="col-md-5ths">
					<a href="#installation-instructions" aria-controls="installation-instructions" role="tab" data-toggle="tab">	Installation Instructions and Specifications</a>
				</li>
				<li role="presentation" class="col-md-5ths">
					<a href="#msds" aria-controls="msds" role="tab" data-toggle="tab">MSDS</a>
				</li>
				<li role="presentation" class="col-md-5ths">
					<a href="#warranty" aria-controls="warranty" role="tab" data-toggle="tab">Warranty</a>
				</li>
			</ul>

			<!-- Tab Panels -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="home">
					<p>The perfect product for all your bonding and sealing jobs with unique characteristics</p>

							<ul>
								<li>Can be painted immediately after application</li>
								<li>Easy to apply in all conditions </li>
								<li>Fast curing</li>
								<li>Does not smell, solvent free </li>
								<li>No primer needed </li>
							</ul>

							<p>Applications include</p>

							<ul>
								<li>Repairing leaks (even under water)</li>
								<li>Bonding skirting-boards</li>
								<li>Filling of cracks</li>
								<li>Bonding of paintings and mirrors to walls</li>
							</ul>
				</div>
				<div role="tabpanel" class="tab-pane" id="installation-videos"><?php lorem_ipsum(); ?></div>
				<div role="tabpanel" class="tab-pane" id="installation-instructions"><?php lorem_ipsum(); ?></div>
				<div role="tabpanel" class="tab-pane" id="msds"><?php lorem_ipsum(); ?></div>
				<div role="tabpanel" class="tab-pane" id="warranty"><?php lorem_ipsum(); ?></div>
			</div>

			</div>

			<div id="accordion" class="panel-group hidden-xl hidden-lg hidden-md">
			    <div class="panel panel-default">
			        <div class="panel-heading">
			            <h4 class="panel-title">
			                <a data-toggle="collapse" data-parent="#accordion" href="#collapseProductOne">Description</a>
			            </h4>
			        </div>
			        <div id="collapseProductOne" class="panel-collapse collapse in">
			            <div class="panel-body">
			                <p>The perfect product for all your bonding and sealing jobs with unique characteristics</p>

							<ul>
								<li>Can be painted immediately after application</li>
								<li>Easy to apply in all conditions </li>
								<li>Fast curing</li>
								<li>Does not smell, solvent free </li>
								<li>No primer needed </li>
							</ul>

							<p>Applications include</p>

							<ul>
								<li>Repairing leaks (even under water)</li>
								<li>Bonding skirting-boards</li>
								<li>Filling of cracks</li>
								<li>Bonding of paintings and mirrors to walls</li>
							</ul>
			            </div>
			        </div>
			    </div>
			    <div class="panel panel-default">
			        <div class="panel-heading">
			            <h4 class="panel-title">
			                <a data-toggle="collapse" data-parent="#accordion" href="#collapseProductTwo">Installation Videos</a>
			            </h4>
			        </div>
			        <div id="collapseProductTwo" class="panel-collapse collapse">
			            <div class="panel-body">
			                <?php lorem_ipsum(); ?>
			            </div>
			        </div>
			    </div>
			    <div class="panel panel-default">
			        <div class="panel-heading">
			            <h4 class="panel-title">
			                <a data-toggle="collapse" data-parent="#accordion" href="#collapseProductThree">Installation Instructions and Specifications</a>
			            </h4>
			        </div>
			        <div id="collapseProductThree" class="panel-collapse collapse">
			            <div class="panel-body">
			                <?php lorem_ipsum(); ?>
			            </div>
			        </div>
			    </div>
			    <div class="panel panel-default">
			        <div class="panel-heading">
			            <h4 class="panel-title">
			                <a data-toggle="collapse" data-parent="#accordion" href="#collapseProductFour">MSDS</a>
			            </h4>
			        </div>
			        <div id="collapseProductFour" class="panel-collapse collapse">
			            <div class="panel-body">
			                <?php lorem_ipsum(); ?>
			            </div>
			        </div>
			    </div>
			    <div class="panel panel-default">
			        <div class="panel-heading">
			            <h4 class="panel-title">
			                <a data-toggle="collapse" data-parent="#accordion" href="#collapseProductFive">Warranty</a>
			            </h4>
			        </div>
			        <div id="collapseProductFive" class="panel-collapse collapse">
			            <div class="panel-body">
			                <?php lorem_ipsum(); ?>
			            </div>
			        </div>
			    </div>
			</div>


			<div class="related-products clearfix">

				<div class="col-md-2 heading">
					<h2>Related item</h2>
				</div>

				<div class="col-md-10 related-products-list clearfix">

					<?php
						$products = array(
							array(
							'image' => 'WB8043_1.jpg',
							'sku' =>   'WB8042',
							'name' => 'Blum TANDEMBOX Intivo D Height 211MM drawer BOXCOVER- integrated BLUMOTION 450MM in Stainless Steel 65KG',
							),
							array(
							'image' => 'WB8043_1.jpg',
							'sku' =>   'WB8043',
							'name' => 'Blum TANDEMBOX Intivo D Height 211MM drawer BOXCOVER- integrated BLUMOTION 500MM in Stainless Steel 65KG',
							),
							array(
							'image' => 'WB8043_1.jpg',
							'sku' =>   'WB8044',
							'name' => 'Blum TANDEMBOX Intivo D Height 211MM drawer BOXCOVER- integrated BLUMOTION 550MM in Stainless Steel 65KG',
							),
							array(
							'image' => 'WB8043_1.jpg',
							'sku' =>   'WB8045',
							'name' => 'Blum TANDEMBOX Intivo D Height 211MM drawer BOXCOVER- integrated BLUMOTION 650MM in Stainless Steel 65KG',
							),
						);
					?>


					<div class = "item-slide hidden-lg hidden-md hidden-sm">

					     <div class="col-xs-12 col-sm-12 col-md-12 image-slider clearfix">
						 	<button class="slider-btn slide-prev col-xs-1 col-sm-1 col-md-1"></button>
							<div class="items col-xs-10 col-sm-10 col-md-10">
								<?php foreach($products as $product) { ?>

								<!-- Related Product Box -->
								<div class="sub-category-item related-products-item col-xs-2">
									<div class="sub-category-container clearfix">

										<a href="step5.php"><img class="img-responsive" src="img/product-kits/<?php echo $product['image']; ?>"></a>

										<div class="row">

											<div class="col-xs-12 col-sm-12 col-md-12">
												<div class="product-heading">
													<h2>
														<a href="step5.php"><?php echo $product['name']; ?></a>
														<span>$100</span>
													</h2>

												</div>
											</div>

										</div>
									</div>
								</div>

							<?php } ?>
							</div>
							<button class="slider-btn slide-next col-xs-1 col-sm-1 col-md-1"></button>

						</div>
					</div>




					<?php
						foreach($products as $product) {
							$link = "step9.php";
					?>

						<!-- Related Product Box -->

						<div class="col-xs-12 col-sm-6 col-md-3 sub-category-item related-products-item hidden-xs">
							<div class="sub-category-container clearfix">

								<a href="step5.php"><img class="img-responsive feature-image" src="img/product-kits/<?php echo $product['image']; ?>"></a>

								<div class="row">

									<div class="col-xs-9 col-sm-9 col-md-9">
										<div class="product-heading">
											<h2><a href="step5.php"><?php echo $product['name']; ?></a></h2>
											<div><span class="grid-orange-border"></span></div>
										</div>
									</div>

									<div class=" col-xs-3 col-sm-3 col-md-3 compact-favourites"><a href="#"><img src="img/icons/favourite_star.png"></a></div>

								</div>

								<div class="row">

									<div class="col-xs-5 col-sm-6 col-md-5 product-code"><?php echo $product['sku']; ?></div>

									<div class="col-xs-4 col-sm-3 col-md-4 product-qty">
										<input type="number" value="1" />
									</div>

									<div class="col-xs-3 col-sm-3 col-md-3 compact-add-to-cart">
										<a href="#"><img src="img/icons/shopping_cart_orange.png"></a>
									</div>

								</div>

							</div>
						</div>

					<?php
						}
					?>

				</div>

			</div>

			<?php output_social_links(); ?>

		</div>

	</div>

</div>

<?php
  // Output footer and we're done!
  output_footer();
?>