<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container category-page">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-xs hidden-sm">

	        <?php category_sidebar(); ?>
	        
		</div>
		
		<div class="main-content clearfix">

			<img class="img-responsive" src="img/promo_banner_two.jpg">

			<h1 class="main-header">Kits - Order Complete Sets</h1>
			
			<div class="col-xs-12 category-list">
				
				<!-- Category Box -->
				<div class="col-xs-12 col-sm-12 col-md-12 category-item product-striplox">
					
					<div class="col-xs-12 col-sm-4 col-md-4 feature-image">
						<img class="img-responsive" src="img/home-grid/home-grid-specials.jpg">
					</div>

					<div class="col-xs-12 col-sm-8 col-md-8 category-body">

						<div class="product-heading">
							<h2>TANDEMBOX Intivo</h2>
							<div><span class="grid-orange-border"></span></div>	
						</div>
						
						<div class="col-md-5 product-links">
							<ul>
								<li><a href="#">Standard Drawers</a></li>
								<li><a href="#">INNER Drawers</a></li>
								<li><a href="#">Special Application Drawers</a></li>
							</ul>
						</div>

					</div>

				</div>

				<!-- Category Box -->
				<div class="col-xs-12 col-sm-12 col-md-12 category-item product-sige">
					
					<div class="col-xs-12 col-sm-4 col-md-4 feature-image">
						<img class="img-responsive" src="img/home-grid/home-grid-perfect-motion.jpg">
					</div>

					<div class="col-xs-12 col-sm-8 col-md-8 category-body">

						<div class="product-heading">
							<h2>TANDEMBOX Antaro</h2>
							<div><span class="grid-orange-border"></span></div>	
						</div>
						
						<div class="col-md-5 product-links">
							<ul>
								<li><a href="#">Standard Drawers</a></li>
								<li><a href="#">INNER Drawers</a></li>
								<li><a href="#">Special Application Drawers</a></li>
								<li><a href="#">Standard Drawers</a></li>
							</ul>
						</div>

						<div class="col-md-5 product-links">
							<ul>
								<li><a href="#">Standard Drawers</a></li>
								<li><a href="#">INNER Drawers</a></li>
								<li><a href="#">Special Application Drawers</a></li>
							</ul>
						</div>

					</div>

				</div>

				<!-- Category Box -->
				<div class="col-xs-12 col-sm-12 col-md-12 category-item product-blum">
					
					<div class="col-xs-12 col-sm-4 col-md-4 feature-image">
						<img class="img-responsive" src="img/home-grid/home-grid-virtual-showroom.jpg">
					</div>

					<div class="col-xs-12 col-sm-8 col-md-8 category-body">

						<div class="product-heading">
							<h2>LEGRABOX Pure</h2>
							<div><span class="grid-orange-border"></span></div>	
						</div>
						
						<div class="col-md-5 product-links">
							<ul>
								<li><a href="#">Standard Drawers</a></li>
								<li><a href="#">INNER Drawers</a></li>
								<li><a href="#">Special Application Drawers</a></li>
							</ul>
						</div>

					</div>

				</div>

				<!-- Category Box -->
				<div class="col-xs-12 col-sm-12 col-md-12 category-item product-wesco">
					
					<div class="col-xs-12 col-sm-4 col-md-4 feature-image">
						<img class="img-responsive" src="img/home-grid/home-grid-roller-shutters.jpg">
					</div>

					<div class="col-xs-12 col-sm-8 col-md-8 category-body">

						<div class="product-heading">
							<h2>TANDEMBOX Antaro &amp; INTIVO WITH TIP-ON</h2>
							<div><span class="grid-orange-border"></span></div>	
						</div>
						
						<div class="col-md-5 product-links">
							<ul>
								<li><a href="#">Standard Drawers</a></li>
								<li><a href="#">INNER Drawers</a></li>
								<li><a href="#">Special Application Drawers</a></li>
							</ul>
						</div>

					</div>

				</div>

			</div>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>