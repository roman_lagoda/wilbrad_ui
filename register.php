<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container">

	<div class="main-content">

		<h1 class="main-header sub-header">Register</h1>
        <p>Already have an account? <a href="#">Login here</a></p>

        <form class="styled-form">
            <div class="row register-forms">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    
                    <div class="form-group">
                        <input type="text" class="form-control" id="first_name" placeholder="First name*" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="surname" placeholder="Surname*" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="company_name" placeholder="Company name">
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="address" placeholder="Address*">
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="address2">
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="suburb" placeholder="Suburb">
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="postcode" placeholder="Postcode">
                        <p class="input-note">Note: We are unable to deliver to PO Boxes</p>
                    </div>

                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-md-offset-1">
                    
                    <div class="form-group">
                        <label for="country">Country</label>
                        <select id="country" name="country" class="form-control">
                            <option value="2">Somewhere</option>
                            <option value="2">Somewhere</option>
                            <option value="2">Somewhere</option>
                            <option value="2">Somewhere</option>
                            <option value="2">Somewhere</option>
                            <option value="3" selected>Australia</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="state">State</label>
                        <select id="state" name="state" class="form-control">
                            <option value="2">Somewhere</option>
                            <option value="2">Somewhere</option>
                            <option value="2">Somewhere</option>
                            <option value="2">Somewhere</option>
                            <option value="2">Somewhere</option>
                            <option value="3" selected>Victoria</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="phone" placeholder="Phone*">
                    </div>

                    <div class="form-group">
                        <input type="email" class="form-control" id="email" placeholder="Email*">
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" id="password" placeholder="Password*">
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password*">
                    </div>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-1 flex-vertical-align">
                    
                    <div class="captcha-fields">
                        <h2>Please enter the characters below as you see it</h2>
                        <p>[CAPTCHA IMAGE HERE]</p>
                        <p>* By clicking "Create Account", you agree to the <a href="#">Terms &amp; Conditions</a></p>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-offset-6 col-sm-6 col-md-2 col-md-offset-10 create-account"><button class="orange-fill btn-block" type="submit">Create Account</button></div>
            </div>
		</form>

	</div>

</div>

<?php
  // Output footer and we're done!
  output_footer();
?>