<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>

<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-sm hidden-xs">

			<?php output_sidebar('downloads', 5); ?>

		</div>

		<div class="main-content">

			<h1 class="main-header">Flyers &amp; Brochures</h1>

			<div class="row">
				<div class='clearfix'>

				<?php

					$brochures = array(
						array(
							'image' 	=> 'AVENTOS HK series programme brochure-1-1',
							'name' 		=> 'AVENTOS HK series programme brochure',
						),
						array(
							'image' 	=> 'AVENTOS MITRED & REBATED APPLICATIONS-1-1',
							'name' 		=> 'AVENTOS Mitred &amp; Rebated Applications',
						),
						array(
							'image' 	=> 'AVENTOS',
							'name' 		=> 'AVENTOS',
						),
						array(
							'image' 	=> 'Blum_narrow_cabinet_solution_-_technical_data_sheet',
							'name' 		=> 'Blum Narrow Cabinet Solution - Technical Data Sheet',
						),
						array(
							'image' 	=> 'clip top blumotion_1',
							'name' 		=> 'Blumotion Clip Top',
						),
						array(
							'image' 	=> 'legrabox pure program',
							'name' 		=> 'Legrabox Pure Program',
						),
						array(
							'image' 	=> 'LEGRABOX pure',
							'name' 		=> 'LEGRABOX Pure',
						),
						array(
							'image' 	=> 'MOVENTO runner system',
							'name' 		=> 'MOVENTO Runner System',
						),
						array(
							'image' 	=> 'ORGA-LINE for TANDEMBOX',
							'name' 		=> 'ORGA-LINE for TANDEMBOX',
						),
						array(
							'image' 	=> 'ORGA-LINE',
							'name' 		=> 'ORGA-LINE',
						),
						array(
							'image' 	=> 'servo drive uno',
							'name' 		=> 'Servo Drive Uno',
						),
						array(
							'image' 	=> 'SERVO-DRIVE flex programme brochure',
							'name' 		=> 'SERVO-DRIVE Flex Programme Brochure',
						),
					);

					$index = -1;

					foreach($brochures as $brochure)
					{
						$index++;

						// Every 4 items, add a new row container so videos with more text in their name don't break ones that are placed below it.
						if($index === 4)
						{
							$index = 0;
							echo "</div><div class='clearfix'>";
						}
				?>

					<div class="col-xs-6 col-sm-3 grid-block brochures-grid-block">
						<a href="#" class="feature-link">
							<img class="img-responsive feature-image" src="img/flyers-brochures/<?php echo $brochure['image']; ?>.jpg">
							<h2 class="feature-heading"><?php echo $brochure['name']; ?></h2>
							<div><span class="grid-orange-border"></span></div>
						</a>
					</div>

				<?php
					}

					// Make sure the row is closed off even if we don't reach an even amount of columns for this row
					if($index !== 0 && $index < 4) { echo "</div>"; }
				?>

			</div>

			<?php output_social_links(); ?>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>