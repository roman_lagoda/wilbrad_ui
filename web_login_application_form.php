<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container">

	<div class="main-content application-form">

		<h1 class="main-header">Web Login Application Form</h1>
        
        <p>EXISITING ACCOUNT CUSTOMERS ONLY. Retail customers can <a href="#">register here</a></p>
        <p>This is a web login application for existing account customers. This is not a trading account application. If you wish you apply for a trading  account please fill in the credit <a href="#">application form</a> and send back to you local branch</p>
        <p>Please complete the form below. Your password application will be completed as soon as possible after you submit the form. Once  registered, you are able to order online using the Wilson and Bradley ordering facility</p>
        
        <h3>Do you have an existing trading account with Wilson & Bradley Pty Ltd?</h3>

        <form class="styled-form">
            <div class="row main-form">
                <div class="col-md-4">
                    
                    <div class="row form-padding">
                        <div class="col-md-6">

                            <div class="radio">
                                <label for="existingAccount1">
                                    <input type="radio" name="existingAccount" id="existingAccount1" value="yes" checked>
                                    Yes
                                </label>
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="radio">
                                <label for="existingAccount2">
                                    <input type="radio" name="existingAccount" id="existingAccount2" value="no">
                                    No
                                </label>
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="account_code" placeholder="Wilson & Bradley Account Code (If known)" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="company_name" placeholder="Company Name" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="abn" placeholder="ABN Number" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="phone_number" placeholder="Phone Number" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="fax_number" placeholder="Fax Number" required>
                    </div>

                    <div class="form-group">
                        <input type="email" class="form-control" id="email" placeholder="Email*" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="contact_person" placeholder="Contact Person" required>
                    </div>

                </div>
                <div class="col-md-4">
                    
                    <h3 class="form-padding">Postal and Invoicing Address</h3>

                    <div class="same-as-postal"></div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="street" placeholder="Street" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="suburb_town" placeholder="Suburb/Town" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="state" placeholder="State" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="postcode" placeholder="Postcode" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="country" placeholder="Country" required>
                    </div>

                </div>
                <div class="col-md-4">
                    
                    <h3 class="form-padding">Delivery Address</h3>

                    <div class="checkbox same-as-postal">
                        <label>
                            <input type="checkbox" value="same_as_postal" checked>
                            Same as postal and invoicing Address
                        </label>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="street" placeholder="Street" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="suburb_town" placeholder="Suburb/Town" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="state" placeholder="State" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="postcode" placeholder="Postcode" required>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="country" placeholder="Country" required>
                    </div>

                    <p class="bold">Please enter the characters below as you see it</p>

                    <div class="row">
                        <div class="col-md-8 col-md-offset-4">
                            <p>[CAPTCHA IMAGE]</p>
                            <button class="orange-fill btn-block" type="submit">Submit</button>
                        </div>
                    </div>

                </div>
            </div>
		</form>

        <?php output_social_links(); ?>

	</div>

</div>

<?php
  // Output footer and we're done!
  output_footer();
?>