      <!-- Newsletter subscribe form -->
      <div class="subscribe-form dark-row-full-width">
        <div class="container">
          <div class="row">
            <form role="form">

              <div class="col-md-4">
                <h2>Receive our latest &amp; exclusive offers</h2>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <input type="text" name="name" placeholder="Name" class="form-control">
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <input type="text" name="email" placeholder="Email" class="form-control">
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <input type="text" name="postcode" placeholder="Postcode" class="form-control">
                </div>
              </div>

              <div class="col-md-2">
                <button type="submit" class="btn btn-block btn-default">Subscribe</button>
              </div>

            </form>
          </div>
        </div>
      </div>

      <!-- Office details section -->
      <div class="office-info responsive-trigger">
        <div class="container">

          <div class="row">

            <div class="col-xs-12 col-md-2">

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Melbourne
                    </a>
                  </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <p>
                      94 Bell Street<br>
                      Preston VIC 3072<br>
                      P. <a href="tel:+61394958900">03 9495 8900</a><br>
                      F. <a href="tel:+61394168878">03 9416 8878</a>
                    </p>
                  </div>
                </div>
              </div>

            </div>

            <div class="col-xs-12 col-md-2">

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Sydney
                    </a>
                  </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                  <div class="panel-body">
                    <p>
                      Unit4,<br>
                      3 Basalt Road Greystanes<br>
                      NSW 2145<br>
                      P. <a href="tel:+61288632700">02 8863 2700</a><br>
                      F. <a href="tel:+61296368822">02 9636 8822</a>
                    </p>
                  </div>
                </div>
              </div>

            </div>

            <div class="col-xs-12 col-md-2">

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Hobart
                    </a>
                  </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                  <div class="panel-body">
                    <a href="mailto:sales@wilbrad.com.au">sales@wilbrad.com.au</a>
                  </div>
                </div>
              </div>

            </div>

            <div class="col-xs-12 col-md-2">

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFour">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      Queensland
                    </a>
                  </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                  <div class="panel-body">
                    <p>
                      2/60 Enterprise Place<br>
                      Tingalpa QLD 4173<br>
                      P. <a href="tel:+61738908611">07 3890 8611</a><br>
                      F. <a href="tel:+61738903375">07 3890 3375</a>
                    </p>
                  </div>
                </div>
              </div>

            </div>

            <div class="col-xs-12 col-md-2">

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFive">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                      Perth
                    </a>
                  </h4>
                </div>
                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                  <div class="panel-body">
                    <p>
                      43 Prosperity Ave<br>
                      Wangara WA 6065<br>
                      P. <a href="tel:+61893032644">08 9303 2644</a><br>
                      F. <a href="tel:+61893039788">08 9303 9788</a>
                    </p>
                  </div>
                </div>
              </div>

            </div>

            <div class="col-xs-12 col-md-2">

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSix">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                      Adelaide
                    </a>
                  </h4>
                </div>
                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                  <div class="panel-body">
                    <p>
                      41 Weaver Street<br>
                      Edwardstown SA 5039<br>
                      P. <a href="tel:+61882763800">08 8276 3800</a><br>
                      F. <a href="tel:+61882763755">08 8276 3755</a>
                    </p>
                  </div>
                </div>
              </div>

            </div>

          </div>

        </div>
      </div>

      <!-- Footer -->
      <footer id="main-footer" class="dark-row-full-width responsive-trigger">
        <div class="container">

          <div class="row">
            
            <div class="col-xs-12 col-md-2">

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFooterOne">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" href="#collapseFooterOne" aria-expanded="true" aria-controls="collapseFooterOne">
                      Customer Service
                    </a>
                  </h4>
                </div>
                <div id="collapseFooterOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFooterOne">
                  <div class="panel-body">
                    <ul>
                      <li><a href="#">Delivery &amp; Returns</a></li>
                      <li><a href="#">Buying Guides</a></li>
                      <li><a href="#">Payment Methods</a></li>
                    </ul>
                  </div>
                </div>
              </div>

            </div>

            <div class="col-xs-12 col-md-2">

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFooterTwo">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" href="#collapseFooterTwo" aria-expanded="false" aria-controls="collapseFooterTwo">
                      About Us
                    </a>
                  </h4>
                </div>
                <div id="collapseFooterTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFooterTwo">
                  <div class="panel-body">
                    <ul>
                      <li><a href="#">Testimonials</a></li>
                      <li><a href="#">Trading Terms</a></li>
                      <li><a href="#">Privacy Policy</a></li>
                    </ul>
                  </div>
                </div>
              </div>

            </div>

            <div class="col-xs-12 col-md-2">

              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFooterThree">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" href="#collapseFooterThree" aria-expanded="false" aria-controls="collapseFooterThree">
                      Wilbrad Reviews
                    </a>
                  </h4>
                </div>
                <div id="collapseFooterThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFooterThree">
                  <div class="panel-body">
                    <ul>
                      <li><a href="#">FAQ</a></li>
                      <li><a href="#">Contact Us</a></li>
                      <li><a href="#">Gift Vouchers</a></li>
                    </ul>
                  </div>
                </div>
              </div>

            </div>

            <div class="col-xs-12 col-sm-6 col-md-4">
              <img class="cards-accepted img-responsive" src="img/cards-accepted.png">
            </div>

            <div class="col-xs-12 col-sm-6 col-md-2">
              <h2 class="visible-md-block visible-lg-block">Join Us</h2>
              <div class="footer-social-icons">
                <a href="#"><img src="img/icons/icon-footer-facebook.png"></a>
                <a href="#"><img src="img/icons/icon-footer-twitter.png"></a>
                <a href="#"><img src="img/icons/icon-footer-youtube.png"></a>
              </div>
            </div>

          </div>

          <div class="row copyright-row">
            
            <div class="col-xs-6">
              Wilson and Bradley 2016
            </div>

            <div class="col-xs-6 align-right">
              All rights reserved
            </div>

          </div>

        </div>
      </footer>

      <!-- Page scripts -->
      <script src="js/vendor/jquery-1.11.2.min.js"></script>
      <script src="js/vendor/bootstrap.min.js"></script>
      <script src="js/vendor/jquery.backstretch.min.js"></script>
      <script src="js/plugins.js"></script>
      <script src="js/main.js"></script>

    </body>
</html>