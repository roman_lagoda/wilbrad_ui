<?php
/*
	Super simple template output. HTML is segmented into php functions so we can simply use:

		output_header();
		output_footer();
		output_sidebar(array(array('href link', 'link name'), array('href link', 'link name')));

    This file will hold variables containing links for the nav menus so templated php and actual
    data for the navigation links can be seperated and easier to find when the time comes.

*/

// --------------------------------------------------------
// Output main header
// --------------------------------------------------------
function output_header()
{
    include('header.php');
}

// --------------------------------------------------------
// Output main footer
// --------------------------------------------------------
function output_footer()
{
    include('footer.php');
}

// --------------------------------------------------------
// Output main footer
// --------------------------------------------------------
function output_social_links()
{
    include('social_links.php');
}

// --------------------------------------------------------
// Placeholder sidebars
// --------------------------------------------------------
function category_sidebar()
{
    ?>

    <ul>
        <li><a href="#">Specials</a></li>
        <li><a href="#">Blum Products - Perfecting Motion</a></li>
        <li>
            <a href="#" class="active">Cabinet Hardware - For All Spaces</a>

            <ul>
                <li><a href="#">Abrasives</a></li>
                <li><a href="#">Adhesives</a></li>
                <li><a href="#">Bathroom Partition Hardware</a></li>
                <li><a href="#">Bolts, Latches &amp; Brackets</a></li>
                <li><a href="#">Buffers</a></li>
                <li><a href="#">Castors &amp; Guides</a></li>
                <li><a href="#">Catches</a></li>
                <li><a href="#">Connector Fittings &amp; Clips</a></li>
                <li><a href="#">Consolidated Veneers &amp; T-Section</a></li>
                <li><a href="#">Cover Caps &amp; Closures</a></li>
                <li><a href="#">Cutlery Trays, Dividers &amp; Acc</a></li>
                <li><a href="#">Drawer Systems &amp; Runners</a></li>
                <li><a href="#">Fastening Systems</a></li>
                <li><a href="#">Gun Brads and Staples</a></li>
                <li><a href="#">Handles</a></li>
                <li><a href="#">Hinges</a></li>
                <li><a href="#">Hooks</a></li>
                <li><a href="#">Ironing Board &amp; Accessories</a></li>
                <li><a href="#">Kitchen Bins</a></li>
                <li><a href="#">Kitchen Fittings</a></li>
                <li><a href="#">LED Lighting- L &amp; S</a></li>
                <li><a href="#">LED Lights- Battery Operated</a></li>
                <li><a href="#">LED Lighting- Strip Lighting</a></li>
                <li><a href="#">Legs</a></li>
                <li><a href="#">Locks</a></li>
                <li><a href="#">Office Fittings</a></li>
                <li><a href="#">Safety Products</a></li>
                <li><a href="#">Screws</a></li>
                <li><a href="#">Screwdrivers &amp; Driver Bits</a></li>
                <li><a href="#">Shelving</a></li>
                <li><a href="#">Stays</a></li>
                <li><a href="#">STRIPLOX Products</a></li>
                <li><a href="#">Thingamejig Precision Tools</a></li>
                <li><a href="#">Tools</a></li>
                <li><a href="#">Towel Rails</a></li>
                <li><a href="#">Tracks Folding &amp; Sliding</a></li>
                <li><a href="#">Vents</a></li>
                <li><a href="#">Vinyl Wrapped Drawers</a></li>
                <li><a href="#">Wardrobe Systems &amp; Fittings</a></li>
                <li><a href="#">Wire Baskets</a></li>
                <li><a href="#">Wire Ware Range-Sige Infinity Plus</a></li>
                <li><a href="#">Wire Ware &amp; Corner Solutions</a></li>
                <li><a href="#">Zipbolts</a></li>
                <li><a href="#">Miscellaneous</a></li>
            </ul>
        </li>
        <li><a href="#">Aluminium Doors &amp; Rollershutters</a></li>
    </ul>

    <h1>Kits</h1>

    <ul>
        <li><a href="#">TANDEMBOX Intivo</a></li>
        <li><a href="#">TANDEMBOX Antaro</a></li>
        <li><a href="#">LEGRABOX Pure</a></li>
        <li><a href="#">TANDEMBOX Antaro &amp; Intivo with TIP-ON</a></li>
        <li><a href="#">SERVO-DRIVE bank</a></li>
        <li><a href="#">TANDEMBOX antaro Drip Trays</a></li>
        <li><a href="#">TANDEMBOX intivo Drip Trays</a></li>
        <li><a href="#">TANDEMBOX antaro SPACE TOWER</a></li>
        <li><a href="#">TANDEMBOX intivo SPACE TOWER</a></li>
        <li><a href="#">TANDEMBOX intivo Standard Drawers with Wesco Pull Boy Bin</a></li>
        <li><a href="#">TANDEMBOX antaro Standard Drawers with Wesco Pull Boy Bin</a></li>
        <li><a href="#">TANDEMBOX intivo Standard Drawers with ORGA-LINE</a></li>
        <li><a href="#">TANDEMBOX antaro Standard Drawers with ORGA-LINE</a></li>
        <li><a href="#">TANDEMBOX intivo trade pack kitcodes</a></li>
        <li><a href="#">TANDEMBOX antaro trade pack kitcodes</a></li>
        <li><a href="#">L &amp; S LED Lighting</a></li>
        <li><a href="#">LEGRABOX Pure Standard Drawers with Wesco Pull Boy Bins</a></li>
    </ul>

    <?php
}

// --------------------------------------------------------
// Output the main sidebar
// --------------------------------------------------------
function output_sidebar($type, $active = -1, $sub_active = -1)
{
    $sidebar = "<ul>";
    $menu_array = array();

    // --------------------------------------------------------
    // Find the right sidebar menu for the page
    // --------------------------------------------------------

    // -- DOWNLOADS
    if($type == "downloads")
    {
        $menu_array = array(
            array(
                'name' => 'Application Forms',
                'href' => '#',
            ),
            array(
                'name' => 'Order Forms',
                'href' => '#',
            ),
            array(
                'name' => 'Instructional &amp; Information Videos',
                'href' => '#',
            ),
            array(
                'name' => 'Installation Instructions',
                'href' => '#',
            ),
            array(
                'name' => 'Material Safety Data Sheets - MSDS',
                'href' => '#',
            ),
            array(
                'name' => 'Flyers &amp; Brochures',
                'href' => '#',
            ),
            array(
                'name' => 'News &amp; Promotions',
                'href' => '#',
            ),
            array(
                'name' => 'Warranty Cards',
                'href' => '#',
            ),
        );

        // Lists in lists oh yay
        
        // -- Instructional & information videos
        if($active === 2)
        {
            $menu_array[$active]['submenu'] = array(
                array(
                    'name' => 'Accuride Pocket Door Installation',
                    'href' => '#',
                ),
                array(
                    'name' => 'BLUM TANDEMBOX antaro',
                    'href' => '#',
                ),
                array(
                    'name' => 'BLUM TANDEMBOX intivo',
                    'href' => '#',
                ),
                array(
                    'name' => 'BLUM AVENTOS HF',
                    'href' => '#',
                ),
                array(
                    'name' => 'BLUM AVENTOS HK',
                    'href' => '#',
                ),
                array(
                    'name' => 'BLUM AVENTOS HL',
                    'href' => '#',
                ),
                array(
                    'name' => 'BLUM AVENTOS HS',
                    'href' => '#',
                ),
                array(
                    'name' => 'BLUM AVENTOS Servo-Drive',
                    'href' => '#',
                ),
                array(
                    'name' => 'BLUM Servo-Drive',
                    'href' => '#',
                ),
                array(
                    'name' => 'BLUM Tip-on for TANDEMBOX',
                    'href' => '#',
                ),
            );
        }
    }

    // -- LISTS
    if($type == "lists")
    {
        $menu_array = array(
            array(
                'name' => 'Account Details',
                'href' => 'account-details.php',
            ),
            array(
                'name' => 'Purchase Order Status',
                'href' => 'purchase-order-status.php',
            ),
            array(
                'name' => 'Purchase History',
                'href' => 'purchase-history.php',
            ),
            array(
                'name' => 'Favourites',
                'href' => 'favourites.php',
            ),
            array(
                'name' => 'Download Invoices',
                'href' => 'download_invoices.php',
            ),
            array(
                'name' => 'Download Statements',
                'href' => 'download-statements.php',
            ),
            array(
                'name' => 'Account Payment',
                'href' => 'account-payment.php',
            ),
        );
    }

    // -- PRODUCTS
    if($type == "products")
    {
        $menu_array = array(
            array(
                'name' => 'Specials',
                'href' => '#',
            ),
            array(
                'name' => 'Blum Products - Perfecting Motion',
                'href' => '#',
            ),
            array(
                'name' => 'Cabinet Hardware - For All Spaces',
                'href' => '#',
            ),
            array(
                'name' => 'Kits',
                'href' => '#',
            ),
            array(
                'name' => 'Aluminium Doors',
                'href' => '#',
            ),
            array(
                'name' => 'Rollershutters',
                'href' => '#',
            ),
            array(
                'name' => 'Furnipart Handles',
                'href' => '#',
            ),
            array(
                'name' => 'Vertual Showroom',
                'href' => '#',
            ),
            array(
                'name' => 'Blum Online Product',
                'href' => '#',
            ),
            array(
                'name' => 'Configuration',
                'href' => '#',
            ),
            array(
                'name' => 'Media Centre',
                'href' => '#',
            ),
        );

        // Lists in lists oh yay
        
        if($active === 1)
        {
            $menu_array[$active]['submenu'] = array(
                array(
                    'name' => 'Blum TANDEMBOX antaro',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum TANDEMBOX intivo',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum LEGRABOX pure',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum ORGA-LINE drawer inserts',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum AMBIA-LINE',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum TIP-ON syncronisation sets',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum METABOX',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum AVENTOS overhead systems',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum AVENTOS SERVO-DRIVE',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum MOVENTO runner system',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum TIP-ON for MOVENTO system',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum TANDEM for wood',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum TIP-ON for TANDEM for wood',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum runners',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum SERVO-DRIVE',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum SERVO-DRIVE Uno Electrical Opening for Waste Bins',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum SERVO-DRIVE FLEX Electrical Opening for Fridges',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum BLUMOTION for doors',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum TIP-ON for doors',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum hinges',
                    'href' => '#',
                ),
                array(
                    'name' => 'Blum assembly devices and MINIPRESS',
                    'href' => '#',
                ),
            );
        }
    }

    // --------------------------------------------------------
    // Build html menu and add the active element 
    // --------------------------------------------------------
    foreach($menu_array as $index=>$menu)
    {
        $active_css = "";
        if($active > -1 && $active === $index) { $active_css = 'class="active"'; }

        $sidebar .= sprintf("<li><a %s href='%s'>%s</a></li>",
            $active_css,
            $menu['href'],
            $menu['name']
        );

        // Check for a sub-menu
        if(isset($menu['submenu']))
        {
            $sidebar .= '<ul>';

            foreach($menu['submenu'] as $sub_index=>$sub_item)
            {
                $sub_active_css = "";
                if($sub_active > -1 && $sub_active === $sub_index) { $sub_active_css = 'class="active"'; }

                $sidebar .= sprintf("<li><a %s href='%s'>%s</a></li>",
                    $sub_active_css,
                    $sub_item['href'],
                    $sub_item['name']
                );
            }

            $sidebar .= '</ul>';
        }
    }

    $sidebar .= '</ul>';

    // OUTPUT
    echo $sidebar;
}

// --------------------------------------------------------
// Output placeholder text
// --------------------------------------------------------
function lorem_ipsum()
{
    echo "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla elementum malesuada felis, eu bibendum tortor rhoncus sit amet. In hac habitasse platea dictumst. Mauris vestibulum interdum ante, eu gravida nibh bibendum ac. Sed molestie id tortor mollis aliquet. Fusce est elit, iaculis et aliquam a, iaculis eu diam. Sed pellentesque, turpis ut tristique pharetra, purus ligula imperdiet libero, eu varius dui sapien vel nunc. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed dapibus, sapien nec convallis rutrum, massa nibh placerat elit, quis sodales ligula tortor in lacus.</p>

<p>Mauris fermentum fermentum magna, sit amet posuere lacus mattis ut. Donec facilisis, dui quis bibendum lacinia, metus nulla sollicitudin ex, non blandit sem lorem quis mauris. Suspendisse dictum suscipit justo, id aliquam odio ultrices ac. Nunc ut pretium tortor. Aliquam dolor augue, sollicitudin quis iaculis ut, venenatis ac quam. Etiam rutrum rhoncus tortor, ac sagittis nibh volutpat nec. Duis gravida semper dolor convallis scelerisque. Vestibulum in urna magna.</p>

<p>Mauris vitae tincidunt metus, ut tincidunt lorem. Duis in sapien ante. Nulla euismod ante vel libero consequat, eu imperdiet erat facilisis. Vivamus vestibulum massa sit amet blandit pellentesque. Praesent eget eros lectus. Fusce ut nisl lorem. In accumsan arcu sit amet mi tincidunt, id aliquam risus feugiat. Cras malesuada tristique ex at sodales.</p>

<p>Nam libero est, faucibus nec rutrum non, tempor nec neque. Nunc pharetra sapien arcu, eu condimentum tortor posuere vel. Aenean eros augue, egestas id risus non, efficitur efficitur tellus. Cras suscipit et risus vitae convallis. Fusce porttitor imperdiet augue sit amet venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat. Nulla faucibus laoreet urna sed interdum. Fusce vestibulum, urna vitae posuere mattis, felis nibh pharetra leo, vel posuere urna enim eu libero.</p>

<p>Vestibulum nisi urna, tincidunt ultrices neque aliquam, dapibus pretium ante. Cras sagittis vitae purus a molestie. Nam nec enim porta, pharetra elit vitae, blandit leo. Nunc facilisis sem ut magna suscipit volutpat. Maecenas massa risus, porta imperdiet felis nec, commodo vulputate nibh. Nam sit amet quam in mi suscipit lobortis. Pellentesque rutrum sem at commodo tincidunt. Proin laoreet ipsum ut diam pharetra sollicitudin. Cras efficitur egestas commodo. Duis id lobortis sapien. Etiam sit amet odio eros. Mauris imperdiet leo ex. Etiam euismod sagittis maximus.</p>";
}

?>