<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Wilson &amp; Bradley</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <!-- <link rel="stylesheet" href="css/jquery.dataTables.min.css"> -->
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="css/icheck-skin/orange.css">
        <!-- <link rel="stylesheet" href="css/jplist.core.min.css"> -->
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

      <!-- Top Navigation bar -->
      <nav class="top-navbar navbar hidden-xs hidden-sm" role="navigation">
        <div class="container">

          <a class="logo-link" href="step1.php"><img src="img/logo.png"></a>

          <!-- Left side is fixed, right side is fluid width -->
          <div class="container row-overflow">

            <div id="nav-top-fixed">
              &nbsp;
            </div>

            <div id="nav-container">
              <div id="nav-top-fluid">

                <div class="nav-social hidden-sm">
                  <a href="#"><img src="img/icons/icon-nav-facebook.png"></a>
                  <a href="#"><img src="img/icons/icon-nav-twitter.png"></a>
                  <a href="#"><img src="img/icons/icon-nav-youtube.png"></a>
                </div>

                <div class="nav-search">
                  <form>
                    <input name="search" placeholder="Search">
                  </form>
                </div>

                <div class="nav-cart">
                  <a href="#">
                    <span class="hidden-md">AUD $0.00</span>
                  </a>
                </div>

                <div class="nav-login">
                  <a href="login.php">
                    <span class="hidden-md">Login</span>
                  </a>
                </div>

                <div class="nav-product-config hidden-sm">
                  <a href="#">Blum Online Product Configurator</a>
                </div>

              </div>
            </div>

          </div>

        </div>
      </nav>

      <!-- Bottom Navigation bar -->
      <nav class="navbar bottom-navbar" role="navigation">
        <div class="container">

          <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>

            <div class="nav-logo small-nav visible-xs-block visible-sm-block">
              <a href="#"><img class="img-responsive center-block" src="img/logo.png"></a>
            </div>

            <div class="nav-login small-nav visible-xs-block visible-sm-block">
              <a href="#">&nbsp;</a>
            </div>

            <div class="nav-search small-nav visible-xs-block visible-sm-block">
              <button class="open-searchbar"></button>
            </div>

            <div class="nav-cart small-nav visible-xs-block visible-sm-block">
              <a href="#"></a>
            </div>

            <form class="mobile-searchbar visible-xs-block visible-sm-block">
              <input type="text">
              <button type="submit">Search</button>
            </form>

          </div>

          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="hidden-md hidden-lg">
              	<a href="#">Our Products <span class="pull-right glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></a>
              	<div class="sub-menu">
	                 <?php category_sidebar(); ?>
	              </div>

              </li>
              <li class="hidden-sm hidden-xs">
              	<a href="#">Our Products</a>
              </li>

              <li class="hidden-md hidden-lg">
              	<a href="#">My Online Services <span class="pull-right glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></a>
              	<div class="sub-menu">
                   <?php output_sidebar('lists', 1); ?>
	              </div>

              </li>
              <li class="hidden-sm hidden-xs">
              	<a href="#">My Online Services</a>
              </li>


              <li class="hidden-md hidden-lg">
              	<a href="#">Media Centre <span class="pull-right glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></a>
              	<div class="sub-menu">
	                 <ul>
                     <li><a href="#">Application Forms</a></li>
                     <li><a href="#">Order Forms</a></li>
                     <li><a href="#">Installation Instructions</a></li>
                     <li><a href="#">Instructional &amp; Information Videos</a></li>
                     <li><a href="#">Material Safety Data Sheets</a></li>
                     <li><a href="#">Flyers &amp; Brochures</a></li>
                     <li><a href="#">News &amp; Promotions</a></li>
                     <li><a href="#">Warranty Cards</a></li>
	                 </ul>
	              </div>

              </li>
              <li class="hidden-sm hidden-xs">
              	<a href="media-centre.php">Media Centre</a>
              </li>

              <li>
              	<a href="#">Contact</a>
              </li>

              <li>
              	<a href="#">Locations</a>
              </li>

              <li>
              	<a href="#">Specials</a>
              </li>
            </ul>
          </div><!--/.navbar-collapse -->

        </div>
      </nav>

      <!-- Mobile layout logo -->
      <!-- <div class="container visible-xs-block visible-sm-block mobile-logo">
        <a href="#"><img class="img-responsive center-block" src="img/logo.png"></a>
      </div> -->
