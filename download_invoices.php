<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>

<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-sm hidden-xs">

	        <?php output_sidebar('lists', 4); ?>

		</div>

		<div class="main-content">

			<h1 class="main-header sub-header">Download Invoices</h1>

			<table class="dataTable" cellspacing="0" width="100%">
				<thead>
					<tr>
						<td>Invoice</td>
						<td>Date</td>
						<td>Total&nbsp;Price</td>
					</tr>
				</thead>

				<tbody>
					<?php for($i=0; $i<200; $i++) { ?>
					<tr>
						<td class="pdf-icon-small"><a href="#"><?php echo 11654 + $i; ?></a></td>
						<td><?php
								$time = new DateTime();
								$time->setDate(date('Y'), date('m'), date('d')+$i);
								echo $time->format('D, jS F Y');
							?>
						</td>
						<td>$<?php echo rand(10, 1000); ?></td>
					</tr>
					<?php } ?>
				</tbody>

			</table>

			<?php output_social_links(); ?>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>