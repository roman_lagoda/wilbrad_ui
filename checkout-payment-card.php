<?php
    // Grab template functions
    require_once('inc/template.php');

    // Output header
    output_header();

    // Page content below:
?>
<div class="container">
    <div class="main-content payment-content">

        <h1 class="main-header">CHECKOUT PAYMENT</h1>

        <!-- Cart items -->
        <div class="clearfix hidden-xs product-list-header">
            <div class="col-sm-1 col-md-1">&nbsp;</div>
            <div class="col-sm-6 col-md-7 align-left">Item Name</div>
            <div class="col-sm-1 col-md-1">Qty</div>
            <div class="col-sm-2 col-md-2">Unit Price<small class="grey-text">ext GST</small></div>
            <div class="col-sm-2 col-md-1">Line Price<small class="grey-text">ext GST</small></div>
        </div>

        <div class="visible-xs-block mobile-product-list-header">Cart</div>

        <?php for($i=0; $i<4; $i++) { ?>

            <div class="clearfix product-list-row checkout-product-list-row">
                <div class="col-xs-6 col-sm-1 col-md-1 product-image"><img src="img/product-kits/WB8043_1.jpg" class="img-responsive" /></div>
                <div class="col-xs-6 col-sm-6 col-md-7 product-name align-left">Blum TANDEMBOX Intivo D Height 211MM drawer BOXCOVER- integrated BLUMOTION 500MM in Stainless Steel 65KG</div>

                <div class="hidden-xs col-sm-1 col-md-1 product-qty"><input type="number" min="0" value="1" class="text-center"></div>
                <div class="hidden-xs col-sm-2 col-md-2 product-price">$240.8828</div>
                <div class="hidden-xs col-sm-2 col-md-1 product-total" style="padding-right:0">$240.8828</div>

                <div class="col-xs-12 visible-xs-block price-display">
                    <div class="col-xs-4 col-sm-1 col-md-1 product-qty">
                        <span>Qty</span>
                        <input type="number" min="0" value="1" class="text-center">
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-2 product-price">
                        <span>Unit Price</span>
                        $240.8828
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-1 product-total" style="padding-right:0">
                        <span>Line Price</span>
                        $240.8828
                    </div>
                </div>
            </div>

        <?php } ?>


        <!-- Price information -->
        <div class="row col-md-12">

           <div class="col-md-1"></div>

	           <div class="col-xs-12 col-sm-6 col-md-6">

	               <!-- Mobile Price display -->
	               <div class="row price-rows price-rows-nopadding hidden-md hidden-lg  hidden-xs hidden-sm">

	                   <div class="col-xs-6 col-sm-3">
	                       <div>Sub Total</div>
	                       <div class="bold">$240.8828</div>
	                   </div>

	                   <div class="col-xs-6 col-sm-3">
	                       <div class="align-right">Shipping</div>
	                       <div class="bold align-right">$19.80</div>
	                   </div>

	                   <div class="col-xs-6 col-sm-3">
	                       <div>Credit Card Surcharge</div>
	                       <div class="bold">$1.69</div>
	                   </div>

	                   <div class="col-xs-6 col-sm-3">
	                       <div class="align-right">Total</div>
	                       <div class="bold align-right">
	                           $288.62<br>
	                           <small class="no-bold">(Including GST)</small>
	                       </div>
	                   </div>

	               </div>

	               <a href="#" class="save-to-favourites">Save Cart to Favourite</a>

	               <p> Same Day Delivery orders must be finalised between 7am and 11am on a business day. </p>

                 <p class="hidden-xs">Please note all credit card payments will incur a 2% surcharge.</p>

	           </div>

			   <div class="col-md-2 hidden-xs"></div>

	           <div class="col-xs-12 col-sm-6 col-md-3">

	               <!-- Desktop Price display -->
	               <div class="row price-rows price-rows-nopadding">

	                   <div class="col-md-8 col-sm-8 col-xs-8 align-right">Sub Total</div>
	                   <div class="col-md-4 col-sm-4 col-xs-4 bold align-right price-value">$240.8828</div>

	                   <div class="col-md-8 col-sm-8 col-xs-8 align-right">Shipping</div>
	                   <div class="col-md-4 col-sm-4 col-xs-4 bold align-right price-value">$19.80</div>

	                   <div class="col-md-8 col-sm-8 col-xs-8 align-right">Credit Card Surcharge</div>
	                   <div class="col-md-4 col-sm-4 col-xs-4 bold align-right price-value">$1.69</div>

	                   <div class="col-md-8 col-sm-8 col-xs-8 bold align-right">Total</div>
	                   <div class="col-md-4 col-sm-4 col-xs-4 bold align-right price-value">$288.62<br><small class="no-bold">(Including GST)</small></div>

                     <p class="text-right visible-xs"><br>Please note all credit card payments will incur a 2% surcharge.</p>
	               </div>

        </div>


       </div>


        <form action="step15.php" class="styled-form payment-form">

            <!-- Payment Method Selection -->
            <div class="row">

                <div class="col-md-1"></div>

                <div class="col-md-11">

                    <h3>Payment Method</h3>

                    <div class="radio">
                        <label>
                            <input type="radio" name="optionsRadios" id="accountPayment" value="option1">
                            Put on the Account or COD
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="optionsRadios" id="creditPayment" value="option2" checked>
                            Credit Card
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="optionsRadios" id="poliPayment" value="option3">
                            Poli Payment
                        </label>
                    </div>

                </div>

            </div>

            <div class="payment-card">
            </div>

            <div class="row payment-background">

                <div class="payment-card">

                    <!-- Payment Form -->
                    <div class="col-sm-7 col-md-5 col-md-offset-1">

                        <div class="payment-form-container">

                            <img class="checkout-cards-accepted" src="img/cards-accepted.png">

                            <div class="form-group">
                                <select id="cctype" name="cctype" class="form-control half-size">
                                    <option value="2">Mastercard</option>
                                    <option value="3" selected>Visa</option>
                                    <option value="4">American Express</option>
                                    <option value="5">Poli</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="nameOnCard">Name On Card</label>
                                <input type="text" id="nameOnCard" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="cardNumber">Card Number</label>
                                <input type="text" id="cardNumber" class="form-control">
                            </div>

                            <div class="form-group">

                                <label>Card Expiry Date</label>

                                <div class="row">
                                    <div class="col-md-6">
                                        <select id="ccexpmm" name="ccexpmm" class="form-control">
                                            <option value="1">01</option>
                                        </select>
                                    </div>

                                    <div class="col-md-6">
                                        <select id="ccexpyyyy" name="ccexpyyyy" class="form-control">
                                            <option value="2016">2016</option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="cvvSecurityCode">CVV Security Code</label>
                                <input type="text" class="form-control" id="ccvSecrityCode">
                                <a href="#" class="what-is-a-cvv">What is CVV?</a>
                            </div>

                        </div>

                        <div class="poli-info-container">

                            <h2>Banks Supported</h2>
                            <img src="img/poli-logo.png">
                            <p>To pay with Poli simply</p>
                            <ol>
                                <li>Click on the "FINALISE ORDER" button - you will be re-directed to POLi</li>
                                <li>Select your bank and click "Proceed with Payment"</li>
                                <li>Login to your Internet Banking website as normal</li>
                                <li>Select your account you want to transfer funds from and confirm payment details</li>
                                <li>Enter your internet banking password to confirm payment and print your receipt</li>
                                <li>When your payment is complete, you will be automatically re-directed back to your order confirmation</li>
                            </ol>

                        </div>

                    </div>

					<!-- Final order details -->
		            <div class="row finalise-order hidden-lg  hidden-md">
		                <div class="col-sm-7">

		                    <div class="col-sm-4 col-xs-4">
		                        <span class="bold">Total</span>
		                    </div>

		                    <div class="col-sm-8 col-xs-8 align-right">
		                        <span class="bold">$288.62</span><br>
		                        (Including GST)
		                    </div>

		                    <div class="col-sm-12 col-xs-12">
		                        <button type="submit" class="orange-fill btn-block finalise-order-btn">Finalise Order</button>
		                    </div>

		                </div>

		            </div>

                    <!-- Payment gateway security information -->
                    <div class="col-sm-4 col-md-5">

                        <div class="security-info">

                            <h3>Security System</h3>
                            <img src="img/checkout_security_logo.png">
                            <p>Need help with your order?</p>
                            <a href="#">Contact us</a>

                        </div>

                    </div>

                </div>

            </div>

            <!-- Final order details -->
            <div class="row finalise-order hidden-sm hidden-xs">

                <div class="col-md-9"></div>
                <div class="col-md-3">

                    <div class="col-sm-4 col-md-4">
                        <span class="bold">Total</span>
                    </div>

                    <div class="col-sm-8 col-md-8 align-right">
                        <span class="bold">$288.62</span><br>
                        (Including GST)
                    </div>

                    <div class="col-md-12">
                        <button type="submit" class="orange-fill btn-block finalise-order-btn">Finalise Order</button>
                    </div>

                </div>

            </div>

        </form>

        <?php output_social_links(); ?>

    </div>
</div>

<?php
    // Output footer and we're done!
    output_footer();
?>
