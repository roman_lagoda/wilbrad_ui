<?php

function directory_to_array($string, $array)
{
	$outputString = "";

	foreach($array as $item)
	{
		$extension_removed = substr($item, 0, count($item)-5);
		$outputString .= sprintf($string."\n", $item, $extension_removed);
	}

	return $outputString;
}

$directory = getcwd()."/img/install-instructions";
$files1 = scandir($directory);

// $format = "%s\n";

$format = "array(\n";
$format .= "'image' => '%s',\n";
$format .= "'name' => '',\n";
$format .= "),";

// $format = "array(\n";
// $format .= "'image' => '%s',\n";
// $format .= "'logo-class' => '',\n";
// $format .= "'name' => '%s',\n";
// $format .= "),";

$directory_formatted = directory_to_array($format, $files1);

?>

<!doctype html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Wilson &amp; Bradley</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <style type="text/css">

        </style>
    </head>
    <body>

    	<p><?php echo $directory; ?></p>

    	<pre>Raw Directory:
<?php print_r($files1); ?></pre>

    	<pre>Generated Code:
<?php echo $directory_formatted; ?></pre>

    </body>
</html>