<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>

<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-sm hidden-xs">

			<?php output_sidebar('downloads', 3); ?>

		</div>

		<div class="main-content">

			<h1 class="main-header">INSTALLATION INSTRUCTIONS</h1>

			<p>
			Wilson &amp; Bradley installation instructions listing - Click on a document's name to view it.<br>
			Click <a href="https://get.adobe.com/reader/" target="_new">here</a> to download Adobe Acrobat Viewer for free.
			</p>

			<?php

				$instructions = array(
					array(
					'image' => '3_4_carousel_shelf_product_specifications.jpg',
					'name' => 'Accuride Flipper Door / Pocket Door Installion',
					),
					array(
					'image' => '41814_spacemaker_softline_psm__approved-1.jpg',
					'name' => 'Accuride Full Extension Heavy Duty Self-Closing Runners - Installation',
					),
					array(
					'image' => 'a139-2000_specification_sheet.jpg',
					'name' => 'Accuride Full Extension Self-Closing Runners Installation',
					),
					array(
					'image' => 'ambia-line_installation_instructions.jpg',
					'name' => 'Adjustable Table Leg Installation',
					),
					array(
					'image' => 'armstrong_cabinet_glass_door_lock__lagdld_lagdla__product_specifications.jpg',
					'name' => 'Armstrong Cabinet Glass Door Lock (LAGDLD LAGDLA) Product Specifications',
					),
					array(
					'image' => 'armstrong_cam_lock__laclfa_laclfd__product_specifications.jpg',
					'name' => 'Armstrong Cam Lock (LACLFALACFLD) Product Specifications',
					),
					array(
					'image' => 'armstrong_central_lock_frontal_mounted_system__lafc3d_lafc3a__product_specifications.jpg',
					'name' => 'Armstrong Combination Lock (LACOML) Product Specifications',
					),
					array(
					'image' => 'armstrong_central_lock_frontal_mounted_system__lafc4d_lafc4a__product_specifications.jpg',
					'name' => 'Armstrong Drawer Lock (LADL22A LADL22D) Product Specifications',
					),
					array(
					'image' => 'armstrong_combination_lock__lacoml__product_specifications..jpg',
					'name' => 'Armstrong Push Lock(LAPL22A LAPL22D) Product Specifications',
					),
				);

			?>
			<div class="row">
			<?php foreach($instructions as $instruction) { ?>

				<div class="col-xs-6 col-md-4 install-instructions-box">
					<a href="#">
					<img class="img-responsive" style="margin:0 auto" src="img/install-instructions/<?php echo $instruction['image']; ?>">
					<?php echo $instruction['name']; ?></a>
					- <?php echo rand(456, 987); ?> KB
				</div>

			<?php } ?>
			</div>

			<?php output_social_links(); ?>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>
