<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>

<div class="container">

	<div class="main-content">

		<ol class="breadcrumb">
		    <li><a href="#">Cabinet Hardware-For All Spaces</a></li>
		    <li><a href="#">Adhesives</a></li>
		    <li><a href="#">Soudal Adhesives</a></li>
		    <li class="active">Soudal Fix All Flexi 290ml</li>
		</ol>

		<a href="step4.php" class="go-back hidden-xs">Back</a>

		<!-- Product header -->
		<div class="product-header clearfix">
			<div class="col-md-2 product-keycode">
				<h1>SOUFLEX</h1>
			</div>
			<div class="col-md-10 product-title">
				<h1>Soudal Fix All Flexi 290ml</h1>
			</div>
		</div>

		<?php
			$image_products = array(
				array(
				'image' => 'SOUFLEX_1.jpg'
				),
				array(
				'image' => 'SOUTUR_1.jpg'
				),
				array(
				'image' => 'SOUXTR_1.jpg'
				),
				array(
				'image' => 'SOUHT_1.jpg'
				),
			);
			?>

		<div class="product-body clearfix">

			<!-- Information Tabsvertical image slider -->


			<div class="col-xs-4 col-sm-4 col-md-2 image-slider clearfix hidden-xs">

				<button class="slider-btn slider-prev"></button>

				<div class="product-slider-container">
					<?php foreach($image_products as $image) { ?>
					<div>
						<a href="javascript:void(0);"><img src="img/category-list-individual/<?php echo $image['image']; ?>" class="img-responsive"></a>
					</div>
					<?php } ?>
				</div>

				<button class="slider-btn slider-next"></button>

			</div>


			<!-- Main featured image-->



			<div class="col-xs-12 col-sm-8 col-md-5 feature-img clearfix">

					<a href="img/category-list-individual/SOUFLEX_1.jpg" class="image-zoom-btn" data-toggle="lightbox"><img src="img/icons/image-zoom.png"/></a>


					<a class="lightbox" href="img/category-list-individual/SOUFLEX_1.jpg" data-toggle="lightbox">
					   <img src="img/category-list-individual/SOUFLEX_1.jpg" class="img-responsive" />
					</a>

			</div>




			<div class="col-xs-12 clearfix thumb-slider hidden-lg hidden-md hidden-sm">
				<button class="slider-btn thumb-slider-prev col-xs-1"></button>

				<div class="product-slider-container  col-xs-10">
					<?php foreach($image_products as $image) { ?>
					<div class="galImg col-xs-3">
						<a href="javascript:void(0);"><img src="img/category-list-individual/<?php echo $image['image']; ?>" class="img-responsive"></a>
					</div>
					<?php } ?>
				</div>

				<button class="slider-btn thumb-slider-next col-xs-1"></button>

			</div>





			<!-- Product details, quantity, price -->
			<div class="col-xs-12 col-sm-12 col-md-5 product-details">

					<div class="price">
						Price (ex GST) : $269.7298 <a href="#">(Login)</a>
					</div>

					<table class="table table-bordered table-price">
						<tr>
							<td><label for="quantity_input">Qty</label></td>
							<td>1 - 2</td>
							<td>3 - 4</td>
							<td>5 - 6</td>
							<td>10 +</td>
						</tr>
						<tr>
							<td>Price (ex GST)</td>
							<td>$11.98</td>
							<td>$11.98</td>
							<td>$11.98</td>
							<td>$11.98</td>
						</tr>
					</table>


				<div class="product-buttons clearfix">

					<div class="col-xs-12 col-sm-12 col-md-12">
						<p>Products available for individual purchase unless otherwise stated.</p>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-12">
						<label for="quantity_input">Quantity</label>
						<input type="number" id="quantity_input" value="1" />
					</div>


					<div class="col-xs-12 col-sm-6 text-center add-to-favs">
						<a href="#">Add to favourites</a>
					</div>

					<div class="col-xs-12 col-sm-6  add-to-cart">
						<a href="#" data-toggle="modal" data-target="#myModal">Add to cart</a>
					</div>

				</div>

			</div>

		</div>

		<div class="product-information">

			<!-- Information Tabs -->
			<div class="hidden-sm hidden-xs">
				<ul class="nav nav-tabs product-info-tabs" role="tablist">
					<li role="presentation" class="col-md-5ths active">
						<a href="#home" aria-controls="home" role="tab" data-toggle="tab">Description</a>
					</li>
					<li role="presentation" class="col-md-5ths">
						<a href="#installation-videos" aria-controls="installation-videos" role="tab" data-toggle="tab">Installation Videos</a>
					</li>
					<li role="presentation" class="col-md-5ths">
						<a href="#installation-instructions" aria-controls="installation-instructions" role="tab" data-toggle="tab">Installation Instructions and Specifications</a>
					</li>
					<li role="presentation" class="col-md-5ths">
						<a href="#msds" aria-controls="msds" role="tab" data-toggle="tab">MSDS</a>
					</li>
					<li role="presentation" class="col-md-5ths">
						<a href="#warranty" aria-controls="warranty" role="tab" data-toggle="tab">Warranty</a>
					</li>
				</ul>

				<!-- Tab Panels -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="home">

						<p>The perfect product for all your bonding and sealing jobs with unique characteristics</p>

						<ul>
							<li>Can be painted immediately after application</li>
							<li>Easy to apply in all conditions </li>
							<li>Fast curing</li>
							<li>Does not smell, solvent free </li>
							<li>No primer needed </li>
						</ul>

						<p>Applications include</p>

						<ul>
							<li>Repairing leaks (even under water)</li>
							<li>Bonding skirting-boards</li>
							<li>Filling of cracks</li>
							<li>Bonding of paintings and mirrors to walls</li>
						</ul>

					</div>
					<div role="tabpanel" class="tab-pane" id="installation-videos"><?php lorem_ipsum(); ?></div>
					<div role="tabpanel" class="tab-pane" id="installation-instructions"><?php lorem_ipsum(); ?></div>
					<div role="tabpanel" class="tab-pane" id="msds"><?php lorem_ipsum(); ?></div>
					<div role="tabpanel" class="tab-pane" id="warranty"><?php lorem_ipsum(); ?></div>
				</div>
			</div>


			<div id="accordion" class="panel-group hidden-xl hidden-lg hidden-md">
			    <div class="panel panel-default">
			        <div class="panel-heading">
			            <h4 class="panel-title">
			                <a data-toggle="collapse" data-parent="#accordion" href="#collapseProductOne">Description</a>
			            </h4>
			        </div>
			        <div id="collapseProductOne" class="panel-collapse collapse in">
			            <div class="panel-body">
			                <p>The perfect product for all your bonding and sealing jobs with unique characteristics</p>

							<ul>
								<li>Can be painted immediately after application</li>
								<li>Easy to apply in all conditions </li>
								<li>Fast curing</li>
								<li>Does not smell, solvent free </li>
								<li>No primer needed </li>
							</ul>

							<p>Applications include</p>

							<ul>
								<li>Repairing leaks (even under water)</li>
								<li>Bonding skirting-boards</li>
								<li>Filling of cracks</li>
								<li>Bonding of paintings and mirrors to walls</li>
							</ul>
			            </div>
			        </div>
			    </div>
			    <div class="panel panel-default">
			        <div class="panel-heading">
			            <h4 class="panel-title">
			                <a data-toggle="collapse" data-parent="#accordion" href="#collapseProductTwo">Installation Videos</a>
			            </h4>
			        </div>
			        <div id="collapseProductTwo" class="panel-collapse collapse">
			            <div class="panel-body">
			                <?php lorem_ipsum(); ?>
			            </div>
			        </div>
			    </div>
			    <div class="panel panel-default">
			        <div class="panel-heading">
			            <h4 class="panel-title">
			                <a data-toggle="collapse" data-parent="#accordion" href="#collapseProductThree">Installation Instructions and Specifications</a>
			            </h4>
			        </div>
			        <div id="collapseProductThree" class="panel-collapse collapse">
			            <div class="panel-body">
			                <?php lorem_ipsum(); ?>
			            </div>
			        </div>
			    </div>
			    <div class="panel panel-default">
			        <div class="panel-heading">
			            <h4 class="panel-title">
			                <a data-toggle="collapse" data-parent="#accordion" href="#collapseProductFour">MSDS</a>
			            </h4>
			        </div>
			        <div id="collapseProductFour" class="panel-collapse collapse">
			            <div class="panel-body">
			                <?php lorem_ipsum(); ?>
			            </div>
			        </div>
			    </div>
			    <div class="panel panel-default">
			        <div class="panel-heading">
			            <h4 class="panel-title">
			                <a data-toggle="collapse" data-parent="#accordion" href="#collapseProductFive">Warranty</a>
			            </h4>
			        </div>
			        <div id="collapseProductFive" class="panel-collapse collapse">
			            <div class="panel-body">
			                <?php lorem_ipsum(); ?>
			            </div>
			        </div>
			    </div>
			</div>


			<div class="related-products clearfix">

				<div class="col-md-2 heading">
					<h2>Related item</h2>
				</div>

				<div class="col-md-10 related-products-list clearfix">

					<?php
						$products = array(
							array(
								'name' 		=> 'Soudal Fix All X-TREME Power 290ml',
								'image' 	=> 'SOUXTR_1.jpg',
								'sku' 		=> 'SOUXTR',
							),
							array(
								'name' 		=> 'Soudal Heavy Duty Cartridge Gun',
								'image' 	=> 'SOUHDCG_1.jpg',
								'sku' 		=> 'SOUHT_1',
							),
							array(
								'name' 		=> 'Twisty Swivle Nozzle for Soudal Silicone adhesives Pack of 5',
								'image' 	=> 'SOUSNO_1.jpg',
								'sku' 		=> 'SOUSNO',
							),
							array(
								'name' 		=> 'Soudal Fix All Crystal 290ml Clear',
								'image' 	=> 'SOUCC_1.jpg',
								'sku' 		=> 'SOUCC',
							),
						);
					?>

					<!-- Related Product Slider -->
					<div class = "item-slide hidden-lg hidden-md hidden-sm">

					     <div class="col-xs-12 col-sm-12 col-md-12 image-slider clearfix">
						 	<button class="slider-btn slide-prev col-xs-1 col-sm-1 col-md-1"></button>
							<div class="items col-xs-10 col-sm-10 col-md-10">
								<?php foreach($products as $v=>$product) { ?>

								<!-- Related Product Box -->
								<div class="sub-category-item related-products-item col-xs-2">
									<div class="sub-category-container clearfix">

										<a href="step5.php"><img class="img-responsive" src="img/category-list-individual/<?php echo $product['image']; ?>"></a>

										<div class="row">

											<div class="col-xs-12 col-sm-12 col-md-12">
												<div class="product-heading">
													<h2>
														<a href="step5.php"><?php echo $product['name']; ?></a>
														<span>$100</span>
													</h2>

												</div>
											</div>

										</div>
									</div>
								</div>

							<?php } ?>
							</div>
							<button class="slider-btn slide-next col-xs-1 col-sm-1 col-md-1"></button>

						</div>

<!--
					   <div class = "carousel-inner">

						  <?php foreach($products as $v=>$product) { ?>


							<div class="item sub-category-item related-products-item col-xs-4">
								<div class="sub-category-container clearfix">

									<a href="step5.php"><img class="img-responsive feature-image" src="img/category-list-individual/<?php echo $product['image']; ?>"></a>

									<div class="row">

										<div class="col-xs-9 col-sm-9 col-md-9">
											<div class="product-heading">
												<h2><a href="step5.php"><?php echo $product['name']; ?></a></h2>
												<div><span class="grid-orange-border"></span></div>
											</div>
										</div>

										<div class=" col-xs-3 col-sm-3 col-md-3 compact-favourites"><a href="#"><img src="img/icons/favourite_star.png"></a></div>

									</div>
								</div>
							</div>

						<?php } ?>


					   </div>
-->

					   <!-- Carousel nav -->
<!--
					   <a class = "carousel-control left" href = "#myCarousel" data-slide = "prev">&lsaquo;</a>
					   <a class = "carousel-control right" href = "#myCarousel" data-slide = "next">&rsaquo;</a>
-->

					</div>


					 <!-- Related Product Box -->

					<?php foreach($products as $product) { ?>

						<!-- Related Product Box -->
						<div class="col-xs-12 col-sm-6 col-md-3 sub-category-item related-products-item hidden-xs">
							<div class="sub-category-container clearfix">

								<a href="step5.php"><img class="img-responsive feature-image" src="img/category-list-individual/<?php echo $product['image']; ?>"></a>

								<div class="row">

									<div class="col-xs-9 col-sm-9 col-md-9">
										<div class="product-heading">
											<h2><a href="step5.php"><?php echo $product['name']; ?></a></h2>
											<div><span class="grid-orange-border"></span></div>
										</div>
									</div>

									<div class=" col-xs-3 col-sm-3 col-md-3 compact-favourites"><a href="#"><img src="img/icons/favourite_star.png"></a></div>

								</div>

								<div class="row">

									<div class="col-xs-5 col-sm-6 col-md-5 product-code"><?php echo $product['sku']; ?></div>

									<div class="col-xs-4 col-sm-3 col-md-4 product-qty">
										<input type="number" value="1" />
									</div>

									<div class="col-xs-3 col-sm-3 col-md-3 compact-add-to-cart">
										<a href="#"><img src="img/icons/shopping_cart_orange.png"></a>
									</div>

								</div>

							</div>
						</div>

					<?php } ?>

				</div>

			</div>

			<?php output_social_links(); ?>

		</div>

	</div>

</div>

<!-- Customised Modal -->
<div class="modal fade custom-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Out of stock</h4>
      </div>
      <div class="modal-body">
        <p>
            We're sorry, but this product is out of stock.
        </p>
      </div>
      <div class="modal-footer">
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6"><button type="button" class="dark-btn btn-block" data-dismiss="modal">Close</button></div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>