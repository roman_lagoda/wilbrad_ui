<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Wilson &amp; Bradley</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>

        <div class="container">
            <div class="row" style="margin-top: 20px">

                <div class="col-md-4" style="margin:0 auto;float: none">

                    <h3>Mobile &amp; Tablet Responsive Templates</h3>
                    <ul>
                        <li><a href="home.php">Home</a> </li>
                        <li><a href="login.php">Login</a> </li>
                        <li><a href="category.php">Category</a> </li>
                        <li><a href="subcategory.php">Sub-category</a> </li>
                        <li><a href="checkout-payment-card.php">Checkout payment - card</a> </li>
                        <li><a href="download_invoices.php">Download Invoices</a> </li>
                        <li><a href="installation_videos.php">Installation Videos</a> </li>
                        <li><a href="installation_videos_open.php">Installation Videos (Open)</a> </li>
                        <li><a href="product.php">Product</a> </li>
                        <li><a href="product_kit.php">Product Kit</a> </li>
                        <li><a href="account-details.php">Account Details</a> </li>
                        <li><a href="shopping_cart.php">Shopping Cart</a> </li>
                        <li><a href="download-statements.php">Download Statements</a> </li>
                        <li><a href="media-centre.php">Media Centre</a> </li>
                        <li><a href="flyers-brochures.php">Flyers &amp; Brochures</a> </li>
                        <li><a href="installation-instructions.php">Installation Instructions</a> </li>
                        <li><a href="contact.php">Contact Us</a> </li>
                        <li><a href="warranty_cards.php">Warranty Cards</a> </li>
                        <li><a href="order-details.php">Order Details</a></li>
                        <li><a href="favourites-open.php">Favourites (Open)</a></li>
                        <li><a href="order-confirmation.php">Order Confirmation</a></li>
                        <li><a href="register.php">Register</a></li>
                        <li><a href="web_login_application_form.php">Web Application Form</a></li>
                        <li><a href="checkout_delivery.php">Checkout Delivery</a></li>
                        <li><a href="our-team.php">Our Team</a></li>
                        <li><a href="search.php">Search</a></li>
                        <li><a href="locations.php">Locations</a></li>
                        <li><a href="news-promotions.php">News &amp; Promotions</a></li>

                        <!-- [Finish once all pages signed off] -->
                        <!-- <li><a href="elements.php">Template elements (development only)</a></li> -->
                    </ul>

                    <h3>Needs Redesign</h3>
                    <ul>
                        <li><a href="purchase-history.php">Purchase History</a></li>
                        <li><a href="purchase-order-status.php">Purchase Order Status</a></li>                        <li><a href="purchase-order-details.php">Purchase Order Details</a></li>
                        <li><a href="account-payment.php">Account Payment</a></li>
                        <li><a href="favourites.php">Favourites</a></li>
                    </ul>

                </div>

            </div>
        </div>

    </body>
</html>