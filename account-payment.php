<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>

<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-xs hidden-sm">

			<?php output_sidebar('lists', 6); ?>

		</div>

		<div class="main-content">

			<h1 class="main-header sub-header">Account Payment</h1>

			<p>Please note the following may not reflect your recent payments. It usually takes up to 15 minutes to update.</p>

			<table class="styled-table striped-table account-payment-table" cellspacing="0" width="100%">
		        <thead>
		          <tr>
		            <td class="pay">Pay</td>
		            <td class="align-left invoice">Invoice</td>
		            <td class="date">Date</td>
		            <td class="debit">Debit</td>
		            <td class="outstanding">Outstanding</td>
		            <td class="align-right pay-amount hidden-xs">Payment Amount</td>
		          </tr>
		        </thead>

		        <tbody>

		        	<?php
		        		$invoice = 11246193;
		        		$total = 0;

		        		for($i=0; $i<20; $i++)
		        		{
		        			$invoice++;
		        	?>

			            <tr>
			            	<td class="pay text-right" data-title="Pay"><input type="checkbox"></td>
			            	<td class="align-left invoice" data-title="Invoice"><?php echo $invoice; ?></td>
			            	<td class="date" data-title="Date">                      <span class="hidden-xs">
			            		<?php
      									$time = new DateTime();
      									$time->setDate(date('Y'), date('m'), date('d')+$i);
      									echo $time->format('D, jS F Y');
      								?>                    </span>                    <span class="visible-xs-inline-block">
                      <?php

                        $time = new DateTime();

                        $time->setDate(date('Y'), date('m'), date('d')+$i);

                        echo $time->format('j M Y');

                      ?>

                    </span>

			            	</td>
			            	<td class="debit" data-title="Debit">$<?php echo rand(1050, 3500); ?></td>
			            	<td class="outstanding" data-title="Outstanding">$<?php
				            		$remaining = rand(500, 1200);
				            		$total += $remaining;
				            		echo $remaining;
			            		?>
			            	</td>
			            	<td class="pay-amount hidden-xs" data-title="Payment amount">
			            		<!-- <input type="text" value="<?php echo $remaining; ?>"> -->
			            		<div class="form-group">
      									<label class="sr-only" for="exampleInputAmount_<?php echo $i; ?>">Amount (in dollars)</label>
      									<div class="input-group">
      										<div class="input-group-addon">$</div>
      										<input type="text" class="form-control" id="exampleInputAmount_<?php echo $i; ?>" placeholder="Amount" value="<?php echo $remaining; ?>">
      									</div>
      								</div>
			            	</td>
			            </tr>

		            <?php } ?>		        </tbody>
            <tfoot>

		            <tr class="account-payment-footer">

		            	<td class="debit text-right" colspan="4">Total</td>

		            	<td class="outstanding">$<?php echo $total; ?></td>

		            	<td class="pay-amount hidden-xs">$0.00</td>

		            </tr>

            </tfoot>
		    </table>

		    <div class="row account-payment-checkout">

		    	<div class="col-md-12 prices clearfix">                    <div class="payment-header">Total amount to pay</div>                    <div class="payment-price">$0.00</div>
		    		<div class="payment-header">Credit card surcharge</div>
		    		<div class="payment-price">$0.00</div>
		    	</div>

				<div class="col-md-7 payment">

					<form class="styled-form">

						<div class="form-group payment-email">
                            <input type="email" id="email" class="form-control" placeholder="Enter Email">
                        </div>

                        <p>Send payment receipt to</p>

						<h3>Payment Method</h3>

	                    <div class="radio">
	                        <label>
	                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" checked>
	                            Credit Card
	                        </label>
	                    </div>
	                    <div class="radio">
	                        <label>
	                            <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
	                            Poli Payment
	                        </label>
	                    </div>

                        <div class="form-group">
                            <select id="cctype" name="cctype" class="form-control half-size">
                                <option value="1">Maestro</option>
                                <option value="2">Mastercard</option>
                                <option value="3" selected>Visa</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="nameOnCard">Name On Card</label>
                            <input type="text" id="nameOnCard" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="cardNumber">Card Number</label>
                            <input type="text" id="cardNumber" class="form-control">
                        </div>

                        <div class="form-group">

                            <label>Card Expiry Date</label>

                            <div class="row">
                                <div class="col-md-6">
                                    <select id="ccexpmm" name="ccexpmm" class="form-control">
                                        <option value="1">01</option>
                                    </select>                                                                                            <select id="ccexpyyyy" name="ccexpyyyy" class="form-control">                                                                                                <option value="2016">2016</option>                                                                                            </select>
                                </div>

                                <!-- <div class="col-md-6">
                                    <select id="ccexpyyyy" name="ccexpyyyy" class="form-control">
                                        <option value="2016">2016</option>
                                    </select>
                                </div> -->
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="cvvSecurityCode">CVV Security Code</label>
                            <input type="text" class="form-control" id="ccvSecrityCode">
                            <a href="#" class="what-is-a-cvv">What is CVV?</a>
                        </div>

                        <button type="submit" class="orange-fill btn-block">Pay by Credit Card</button>

					</form>

				</div>

		    </div>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>
