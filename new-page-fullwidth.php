<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container">

	<div class="main-content">

		<h1>New Page - Full Width</h1>

		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla elementum malesuada felis, eu bibendum tortor rhoncus sit amet. In hac habitasse platea dictumst. Mauris vestibulum interdum ante, eu gravida nibh bibendum ac. Sed molestie id tortor mollis aliquet. Fusce est elit, iaculis et aliquam a, iaculis eu diam. Sed pellentesque, turpis ut tristique pharetra, purus ligula imperdiet libero, eu varius dui sapien vel nunc. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed dapibus, sapien nec convallis rutrum, massa nibh placerat elit, quis sodales ligula tortor in lacus.</p>

		<p>Mauris fermentum fermentum magna, sit amet posuere lacus mattis ut. Donec facilisis, dui quis bibendum lacinia, metus nulla sollicitudin ex, non blandit sem lorem quis mauris. Suspendisse dictum suscipit justo, id aliquam odio ultrices ac. Nunc ut pretium tortor. Aliquam dolor augue, sollicitudin quis iaculis ut, venenatis ac quam. Etiam rutrum rhoncus tortor, ac sagittis nibh volutpat nec. Duis gravida semper dolor convallis scelerisque. Vestibulum in urna magna.</p>

		<p>Mauris vitae tincidunt metus, ut tincidunt lorem. Duis in sapien ante. Nulla euismod ante vel libero consequat, eu imperdiet erat facilisis. Vivamus vestibulum massa sit amet blandit pellentesque. Praesent eget eros lectus. Fusce ut nisl lorem. In accumsan arcu sit amet mi tincidunt, id aliquam risus feugiat. Cras malesuada tristique ex at sodales.</p>

		<p>Nam libero est, faucibus nec rutrum non, tempor nec neque. Nunc pharetra sapien arcu, eu condimentum tortor posuere vel. Aenean eros augue, egestas id risus non, efficitur efficitur tellus. Cras suscipit et risus vitae convallis. Fusce porttitor imperdiet augue sit amet venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam erat volutpat. Nulla faucibus laoreet urna sed interdum. Fusce vestibulum, urna vitae posuere mattis, felis nibh pharetra leo, vel posuere urna enim eu libero.</p>

		<p>Vestibulum nisi urna, tincidunt ultrices neque aliquam, dapibus pretium ante. Cras sagittis vitae purus a molestie. Nam nec enim porta, pharetra elit vitae, blandit leo. Nunc facilisis sem ut magna suscipit volutpat. Maecenas massa risus, porta imperdiet felis nec, commodo vulputate nibh. Nam sit amet quam in mi suscipit lobortis. Pellentesque rutrum sem at commodo tincidunt. Proin laoreet ipsum ut diam pharetra sollicitudin. Cras efficitur egestas commodo. Duis id lobortis sapien. Etiam sit amet odio eros. Mauris imperdiet leo ex. Etiam euismod sagittis maximus.</p>

	</div>

</div>

<?php
  // Output footer and we're done!
  output_footer();
?>