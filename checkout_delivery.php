<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>

<div class="container">

	<div class="main-content shipping-details-page">

		<h1 class="main-header sub-header">Checkout Shipping</h1>

		<form class="styled-form row shipping-information">

			<div class="col-xs-12 col-md-4">

				<h1 class="dashed-header">Shipping details</h1>

				<table class="info-table equal-cols shipping-details-table" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td>First name</td>
							<td>M T R Designer Cabinets</td>
						</tr>
						<tr>
							<td>Last name</td>
							<td>M W F C Web MSP</td>
						</tr>
						<tr>
							<td>Company name</td>
							<td></td>
						</tr>
						<tr>
							<td>Billing address</td>
							<td>35 Thornycraft Street</td>
						</tr>
						<tr>
							<td>Suburb</td>
							<td>Campbellfield</td>
						</tr>
						<tr>
							<td>Postcode</td>
							<td>3061</td>
						</tr>
						<tr>
							<td>State</td>
							<td>VIC</td>
						</tr>
						<tr>
							<td>Country</td>
							<td>Australia</td>
						</tr>
						<tr>
							<td>Phone</td>
							<td>03 9088 8811</td>
						</tr>
						<tr>
							<td>Email</td>
							<td>accounts@mtcabinets.com</td>
						</tr>
					</tbody>
				</table>

				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-12">

						<div class="form-group">
							<label></label>
							<input type="text" class="form-control" id="exampleInput1" placeholder="Customer order number">
						</div>

					</div>
					<div class="col-xs-12 col-sm-6 col-md-12">

						<div class="form-group">
							<label for="exampleInput1">Send order confirmation email to (your email)</label>
							<input type="text" class="form-control" id="exampleInput1" placeholder="Email" value="order@mtrcabinets.com">
						</div>

					</div>
				</div>

			</div>

			<div class="col-xs-12 col-md-8">

				<h1 class="dashed-header">Delivery address</h1>

				<div class="row">

					<div class="col-xs-12 col-md-6">

						<div class="radio delivery-type-deliver">
							<label>
								<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
								Deliver to the address below<br>
								<span class="bold">STRICTLY NO GOODS LEFT WITHOUT SIGNATURE</span><br>
								- please be present for delivery order
							</label>
						</div>

						<p class="same-day-delivery">Same Day Delivery is currently unavailable</p>

					</div>

					<div class="col-xs-12 col-md-6">

						<div class="radio delivery-type-pickup">
							<label>
								<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
								Pickup from Melbourne
							</label>
						</div>

						<div class="radio pickup-subselection">
							<label>
								<input type="radio" name="optionsRadiosTwo" id="optionsRadios3" value="option1" checked>
								Customer picking up from counter
							</label>
						</div>

						<div class="radio pickup-subselection">
							<label>
								<input type="radio" name="optionsRadiosTwo" id="optionsRadios4" value="option2">
								Customer sending courier
							</label>
						</div>

					</div>

				</div>

				<div class="row delivery-form-row">

					<div class="col-xs-12 col-md-6">

						<div class="form-group">
							<label>Delivery address</label>
							<input type="text" class="form-control">
						</div>

						<div class="form-group">
							<input type="text" class="form-control">
						</div>

						<div class="form-group">
							<label>Suburb</label>
							<input type="text" class="form-control">
						</div>

						<div class="form-group">
							<label>Postcode</label>
							<input type="text" class="form-control">
						</div>

						<div class="form-group">
							<label>State</label>
							<input type="text" class="form-control">
						</div>

						<div class="form-group">
							<label>Country</label>
							<input type="text" class="form-control">
						</div>

						<div class="form-group">
							<label>Delivery Instructions</label>
							<textarea class="form-control" rows="3"></textarea>
						</div>

					</div>

				</div>

			</div>

		</form>

		<div class="row action-buttons">
			<div class="col-xs-12 col-sm-5 col-md-3"><a href="#" class="dark-btn align-center btn-block">Back to shopping cart</a></div>
			<div class="col-xs-12 col-sm-offset-2 col-sm-5 col-md-offset-6 col-md-3 finish-delivery-btn"><a href="#" class="orange-fill align-center btn-block">Continue</a></div>
		</div>

		<?php output_social_links(); ?>

	</div>

</div>

<?php
  // Output footer and we're done!
  output_footer();
?>