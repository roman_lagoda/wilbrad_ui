<?php
	// Grab template functions
	require_once('inc/template.php');

	// Output header
	output_header();

	// Account details page content below:
?>

<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-sm hidden-xs">
			<?php output_sidebar('lists', 0); ?>
		</div>

		<div class="main-content">

			<h1 class="main-header sub-header">ACCOUNT DETAILS</h1>
			<div style="color: silver; margin-bottom: 15px"><?php echo gmdate("l j F Y h:i:s A e"); ?></div>

			<p>Please note account details are updated daily. To view a live account balance, please view your <a href="#">account payments</a> page.</p>

			<div class="row">
				<div class="col-sm-12">
					<table class="dataTable striped-table equal-cols" cellspacing="0" width="100%">
						<tbody>
							<tr><td>Namekey</td><td>JSSMV1</td></tr>
							<tr><td>Full Name</td><td>John Smith</td></tr>
							<tr><td>Billing Address</td><td>123 Elizabeth st, Melbourne VICTORIA 3000</td></tr>
							<tr><td>Telephone</td><td>0391235555</td></tr>
							<tr><td>Fax</td><td>0399998888</td></tr>
							<tr><td>Email</td><td>email@gmail.com</td></tr>
							<tr><td>Email for order confirmation</td><td>orders@gmail.com</td></tr>
							<tr><td>Delivery Address</td><td>123 Somewhere st, Melbourne VICTORIA 3000</td></tr>
							<tr><td>Business Development Manager</td><td>Jane Smith</td></tr>
							<tr><td>Business Development Manager Email</td><td>manager@wilbrad.com.au</td></tr>
							<tr><td>Warehouse</td><td>Melbourne</td></tr>
						</tbody>
					</table>
				</div>
			</div>
			

			<h3>Outstanding</h3>

			<div class="row">
				<div class="col-sm-6">
					<table class="dataTable striped-table equal-cols" cellspacing="0" width="100%">
						<tbody>
							<tr><td>Credit Limit</td><td>$40,000,000</td></tr>
							<tr><td>Days 90</td><td>$0.0000</td></tr>
							<tr><td>Days 30</td><td>$22,340.2100</td></tr>
							<tr><td>Unallocated</td><td>$0.0000</td></tr>
						</tbody>
					</table>
				</div>

				<div class="col-sm-6">
					<table class="dataTable striped-table equal-cols" cellspacing="0" width="100%">
						<tbody>
							<tr><td>Credit Limit Days</td><td>35</td></tr>
							<tr><td>Days 60</td><td>$0.0000</td></tr>
							<tr><td>Current</td><td>$11,857.8100</td></tr>
							<tr><td>Forward</td><td>$0.0000</td></tr>
							<tr style="background-color: #c96925; color: white"><td>Total</td><td>$0.0000</td></tr>
						</tbody>
					</table>
				</div>
				
				<div class="col-sm-6"></div>

				<div class="col-sm-6">
					<p><br>Pay your account via our <a href="#">online account payment portal</a></p>
				</div>
			</div>

			<?php output_social_links(); ?>

		</div>

	</div>
</div>

<?php
	// Output footer and we're done!
	output_footer();
?>
