<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>

<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-sm hidden-xs">

			<?php output_sidebar('lists', 1); ?>

		</div>

		<div class="main-content">

			<h1 class="main-header">Purchase Order Details</h1>

            <div class="order-details-info clearfix">

                <div class="clearfix hidden-xs product-list-header">
                    <div class="col-xs-4">Order Number</div>
                    <div class="col-xs-4">Date</div>
                    <div class="col-xs-4">Outstanding Value</div>
                </div>

                <div class="clearfix product-list-row">

                    <div class="col-xs-12 col-sm-4">
                        <span class="visible-xs-block bold">Order Number</span>
                        W00115422
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <span class="visible-xs-block bold">Date</span>
                        Mar 24, 2016
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <span class="visible-xs-block bold">Outstanding Value</span>
                        $200
                    </div>
                </div>

            </div>

            <h3>Product List</h3>

            <div class="hidden-xs hidden-sm clearfix product-list-header">
                <div class="col-sm-6 align-left">Item</div>
                <div class="col-sm-3">Quantity</div>
                <!-- <div class="col-sm-1">Ordered</div>
                <div class="col-sm-1">Reserved</div>
                <div class="col-sm-1">Delivered</div> -->
                <div class="col-sm-2">Unit Price</div>
                <div class="col-sm-1">Disc</div>
            </div>

            <div class="visible-xs-block mobile-product-list-header"></div>

            <?php for($i=0; $i<4; $i++) { ?>

                <div class="clearfix product-list-row checkout-product-list-row">

                    <div class="col-xs-6 col-sm-3 col-md-2 product-image">
                      <img src="img/home-grid/home-grid-all-spaces.jpg" class="img-responsive" />
                    </div>
                    <div class="col-xs-6 col-sm-9 col-md-4 product-name align-left">
                      <span class="product-namekey">MBKDK45 <br/></span>
                      Blum METABOX Industrial Pack Of 100. Single Extension Steel Sides 118mm X 450mm. *Sides And Runners Only* 320K4500C15
                    </div>

                    <div class="hidden-sm hidden-xs col-sm-3 price-display">
                      <div class="col-xs-12">
                          <span>Ordered</span> 200
                      </div>
                      <div class="col-xs-12">
                          <span>Reserved</span> 100
                      </div>
                      <div class="col-xs-12">
                          <span>Delivered</span> 100
                      </div>
                    </div>

                    <div class="col-sm-2 hidden-sm hidden-xs">$2.06658 EA</div>
                    <div class="col-sm-1 hidden-sm hidden-xs">10%</div>

                    <div class="col-xs-12 visible-xs visible-sm price-display">
                        <div class="col-xs-4 product-qty">
                            <span>Ordered</span>
                            200
                        </div>
                        <div class="col-xs-4 product-qty">
                            <span>Reserved</span>
                            100
                        </div>
                        <div class="col-xs-4 product-qty">
                            <span>Delivered</span>
                            100
                        </div>
                    </div>

                    <div class="col-xs-12 visible-xs visible-sm price-display">
                        <div class="col-xs-4 product-price">
                            <span>Per</span>
                            EA
                        </div>
                        <div class="col-xs-4 product-price">
                            <span>Unit Price</span>
                            $2.06658
                        </div>
                        <div class="col-xs-4 product-price">
                            <span>Disc</span>
                            10%
                        </div>
                    </div>

                </div>

            <?php } ?>

            <?php output_social_links(); ?>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>