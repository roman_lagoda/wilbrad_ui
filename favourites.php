<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>

<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar">

			<?php output_sidebar('lists', 3); ?>

		</div>

		<div class="main-content">

			<h1 class="main-header sub-header">FAVOURITES</h1>

				<table class="dataTable favourites-table" cellspacing="0" width="100%">
				        <thead>
					   <tr>
					       <td>LIST NAME</td>
					       <td width="10%">NO. OF ITEMS</td>
					       <td>TOTAL PRICE</td>
					       <td>CREATED</td>
					       <td class="hidden-xs">LAST ACCESSED</td>
					   </tr>
					</thead>

					<tbody>
			<?php for ($i=0; $i < 25; $i++) { ?>
					  <tr>
					      <td><a href="favourites-open.php">8 rodman st reservoir</a></td>
					      <td class="text-center">8</td>
					      <td>$1074.10</td>
					      <td>Feb 24 2016</td>
					      <td class="hidden-xs">Feb 24 2016</td>
			<?php } ?>
					</tbody>
				</table>

			<div class="row">
				<div class="col-md-8"></div>
				<div class="col-md-4" style="padding-right:0"><a href="#" class="orange-btn btn-block align-center" style=" margin-top: 20px;">Create a New Favourite</a></div>
			</div>

			<div class="row">
				<?php output_social_links(); ?>
			</div>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>
