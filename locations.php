<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>

<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-sm hidden-xs">
            <ul>
                <li><a href="#" class="active">Melbourne</a></li>
                <li><a href="#">Sydney</a></li>
                <li><a href="#">Hobart</a></li>
                <li><a href="#">Brisbane</a></li>
                <li><a href="#">Perth</a></li>
                <li><a href="#">Adelaide</a></li>
            </ul>

		</div>

		<div class="main-content location-page">

      <h1 class="main-header sub-header">Locations</h1>

			<div class="row">
                <div class="col-md-6">

                    <div class="aus-map">
                        <button id="brisbane">Brisbane</button>
                        <button id="perth">Perth</button>
                        <button id="sydney">Sydney</button>
                        <button id="melbourne" class="active">Melbourne</button>
                        <button id="hobart">Hobart</button>
                    </div>

                </div>
                <div class="col-md-6">

                  <div class="google-map">

                      <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAQlPmRGFxISV0PeJWdhkJKoLWUNtsS7RY'></script>
                      <div id="gmap_container"><div id='gmap_canvas'></div></div>

                  </div>

                    <div class="row">
                        <div class="col-md-6 map-locations">

                            <div class="location melbourne-location active">
                                <h2>Melbourne</h2>
                                <p>
                                    94 Bell St<br>
                                    Preston, 3072<br>
                                    Victoria, Australia<br>
                                    ABN: 40211980801<br>
                                    03 9495 8900<br>
                                    <a href="mailto:sales@wilbrad.com.au">sales@wilbrad.com.au</a>
                                </p>
                            </div>

                            <div class="location brisbane-location">
                                <h2>Brisbane</h2>
                                <p>
                                    2/60 Enterprise Place<br>
                                    Tingalpa QLD 4173<br>
                                    P. <a href="tel:+61738908611">07 3890 8611</a><br>
                                    F. <a href="tel:+61738903375">07 3890 3375</a><br>
                                    <a href="mailto:sales@wilbrad.com.au">sales@wilbrad.com.au</a>
                                </p>
                            </div>

                            <div class="location perth-location">
                                <h2>Perth</h2>
                                <p>
                                    43 Prosperity Ave<br>
                                    Wangara WA 6065<br>
                                    P. <a href="tel:+61893032644">08 9303 2644</a><br>
                                    F. <a href="tel:+61893039788">08 9303 9788</a><br>
                                    <a href="mailto:sales@wilbrad.com.au">sales@wilbrad.com.au</a>
                                </p>
                            </div>

                            <div class="location sydney-location">
                                <h2>Sydney</h2>
                                <p>
                                    Unit4,<br>
                                    3 Basalt Road Greystanes<br>
                                    NSW 2145<br>
                                    P. <a href="tel:+61288632700">02 8863 2700</a><br>
                                    F. <a href="tel:+61296368822">02 9636 8822</a><br>
                                    <a href="mailto:sales@wilbrad.com.au">sales@wilbrad.com.au</a>
                                </p>
                            </div>

                            <div class="location hobart-location">
                                <h2>Hobart</h2>
                                <p>
                                    <a href="mailto:sales@wilbrad.com.au">sales@wilbrad.com.au</a>
                                </p>
                            </div>

                        </div>
                        <div class="col-md-6">

                            <h2>Showroom Opening Hours:</h2>
                            <p>8.30am - 2pm, Mon-Fri</p>
                            <p>Orders can be collected by COURIERS ONLY between 2pm-4:30pm from the despatch area</p>

                        </div>
                    </div>



                </div>
            </div>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>