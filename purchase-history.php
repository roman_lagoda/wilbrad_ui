<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>

<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar">

			<?php output_sidebar('lists', 2); ?>

		</div>

		<div class="main-content">

			<h1 class="main-header sub-header">PURCHASE HISTORY</h1>

			<div class="row">

				<div class="radio">
					<label>
					<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
							                  Online sales history
					</label>
				</div>
				<div class="radio">
					<label>
					<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
							                  All sales history
					</label>
				</div>

			</div>

			<table class="dataTable" cellspacing="0" width="100%">
			        <thead>
				   <tr>
				       <td>WILSON &amp; BRADLEY ORDER NO</td>
				       <td>CUSTOMER ORDER NUMBER</td>
				       <td>DATE</td>
				       <td>TOTAL PRICE</td>
				       <td class="hidden-xs">ADD TO CART</td>
				       <td class="hidden-xs">ADD TO FAVOURITE</td>
				   </tr>
				</thead>

				<tbody>
		<?php for ($i=0; $i < 500; $i++) { ?>
				  <tr>
				      <td><a href="#">W00110652</a></td>
				      <td>J.IRWIN - 44 Winston Drive, Doncaster</td>
				      <td>29 Feb 2016</td>
				      <td>$1586.01</td>
				      <td class="align-center hidden-xs"><a href="#"><img src="img/icons/icon-nav-cart.png" /></a></td>
				      <td class="align-center hidden-xs"><a href="#"><img src="img/icons/favourite_star.png" /></a></td>
		<?php } ?>
				</tbody>
			</table>

			<?php output_social_links(); ?>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>
