<?php
    // Grab template functions
    require_once('inc/template.php');

    // Output header
    output_header();

    // Page content below:
?>

<div class="container">
    <div class="sidebar-content-wrapper">

        <div class="main-sidebar hidden-sm hidden-xs">
            <?php output_sidebar('lists', 3); ?>
        </div>

        <div class="main-content">

            <h1 class="main-header">FAVOURITES</h1>
            <h2>LED Lighting</h2>

            <form>

                <!-- Cart items -->
                <div class="clearfix hidden-xs product-list-header">
                    <div class="col-sm-1 col-md-1"><div class="checkbox"><input type="checkbox" value=""></div></div>
                    <div class="col-sm-offset-1 col-sm-5 col-md-offset-1 col-md-6 align-left">Item Name</div>
                    <div class="col-sm-1 col-md-1">Qty</div>
                    <div class="col-sm-2 col-md-2">Unit Price<small class="grey-text">ext GST</small></div>
                    <div class="col-sm-2 col-md-1">Line Price<small class="grey-text">ext GST</small></div>
                </div>

                <div class="visible-xs-block mobile-product-list-header">
                    <span class="checkbox">
                      <label><input type="checkbox" value=""> &nbsp; Select All</label>
                    </span>
                </div>

                <?php for($i=0; $i<4; $i++) { ?>

                    <div class="clearfix product-list-row favourites-open-product-list-row">
                        <div class="col-sm-1 col-md-1 favourites-checkbox"><div class="checkbox text-right"><input type="checkbox" value=""></div></div>
                        <div class="col-xs-6 col-sm-1 col-md-1 product-image"><img src="img/product-kits/WB8043_1.jpg" class="img-responsive" /></div>
                        <div class="col-xs-6 col-sm-5 col-md-6 product-name align-left">Blum TANDEMBOX Intivo D Height 211MM drawer BOXCOVER- integrated BLUMOTION 500MM in Stainless Steel 65KG</div>

                        <div class="hidden-xs col-sm-1 col-md-1 product-qty"><input type="number" min="0" value="1"></div>
                        <div class="hidden-xs col-sm-2 col-md-2 product-price">$240.8828</div>
                        <div class="hidden-xs col-sm-2 col-md-1 product-total" style="padding-right:0">$240.8828</div>

                        <div class="col-xs-12 visible-xs-block price-display">
                            <div class="col-xs-4 col-sm-1 col-md-1 product-qty">
                                <span>Qty</span>
                                <input type="number" min="0" value="1">
                            </div>
                            <div class="col-xs-4 col-sm-2 col-md-2 product-price">
                                <span>Unit Price</span>
                                $240.8828
                            </div>
                            <div class="col-xs-4 col-sm-2 col-md-1 product-total" style="padding-right:0">
                                <span>Line Price</span>
                                $240.8828
                            </div>
                        </div>
                    </div>

                <?php } ?>

                <div class="row favourites-open-total">
                    <div class="col-xs-offset-4 col-xs-4 col-sm-offset-8 col-sm-2 col-md-offset-8 col-md-2 text-right bold">Total <small class="no-bold">(ex GST)</small></div>
                    <div class="col-xs-4 col-sm-2 text-right bold">$355.30</div>
                </div>

                <div class="row">
                    <div class="col-xs-offset-4 col-xs-4 col-sm-offset-8 col-sm-2 col-md-offset-8 col-md-2 text-right bold">Total <small class="no-bold">(inc GST)</small></div>
                    <div class="col-xs-4 col-sm-2 col-md-2 text-right bold">$390.83</div>
                </div>

                <div class="row favourites-open-buttons">
                    <div class="col-xs-12 col-sm-4 col-md-3"><button type="submit" class="dark-btn btn-default" style="width: 100%">Delete this list</button></div>
                    <div class="col-xs-12 col-sm-offset-4 col-sm-4 col-md-offset-6 col-md-3"><button type="submit" class="orange-fill btn-block add-all-to-cart">Add all to cart</button></div>
                </div>

            </form>

            <?php output_social_links(); ?>

        </div>

    </div>
</div>

<?php
    // Output footer and we're done!
    output_footer();
?>
