<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();
?>
  
<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-sm hidden-xs">
			
	        <?php output_sidebar('downloads', 7); ?>
	        
		</div>
		
		<div class="main-content">

			<h1 class="main-header sub-header">Warranty Cards</h1>

			<table class="styled-table striped-table application-forms-table" cellspacing="0" width="100%">
                <thead>
                    <th></th>
                    <th></th>
                </thead>
				<tbody>
					<?php for($i=0; $i<10; $i++) { ?>
					<tr>
						<td><img src="img/icons/pdf_icon_small.png" /><a href="#">Warranty Card <?php echo $i; ?></a></td>
						<td><?php echo rand(10, 1000); ?>kb</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>

			<?php output_social_links(); ?>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>