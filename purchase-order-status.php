<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>

<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar">

			<?php output_sidebar('lists', 1); ?>

		</div>

		<div class="main-content">

			<h1 class="main-header sub-header">Outstanding Purchase Orders</h1>
			<p>Please select which purchase order you wish to view by clicking on the Order-Number links below</p>

			<table class="dataTable-nofiltering" cellspacing="0" width="100%">
		        <thead>
		          <tr>
		            <td>Wilson &amp; Bradley Order No.</td>
		            <td>Customer Order No.</td>
		            <td>Department</td>
		            <td>Order Date</td>
		          </tr>
		        </thead>

		        <tbody>
		            <tr>
		            	<td><a href="purchase-order-details.php">W00110545</a></td>
		            	<td class="bold">STOCK</td>
		            	<td></td>
		            	<td>Fri, 26th February 2016</td>
		            </tr>
		            <tr>
		            	<td><a href="purchase-order-details.php">S1237301A</a></td>
		            	<td class="bold">ESTRUCT - 20 PERRY ST</td>
		            	<td></td>
		            	<td>Thu, 25th February 2016</td>
		            </tr>
		        </tbody>
		    </table>

		    <?php output_social_links(); ?>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>