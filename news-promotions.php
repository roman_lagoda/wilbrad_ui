<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar  hidden-sm hidden-xs">

			<h1>Sidebar Heading</h1>

			<ul>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li>
	              <a href="#" class="active">Sub Menu</a>
	              <ul>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#" class="active">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	              </ul>
	            </li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	        </ul>
	        
		</div>
		
		<div class="main-content">

			<h1 class="main-header">News &amp; Promotions</h1>

            <div class="news-list">
                <?php for($i=0; $i < 5; $i++) { ?>

                <div class="news-item clearfix">
                    <div class="col-xs-12 col-md-4 news-img">
                        <img class="img-responsive" src="img/promo-news-item.png" />
                    </div>
                    <div class="col-xs-12 col-md-6 news-content">
                        <h2>Welcome to 2016 Promotion</h2>
                        <p>Simply spend $1000 online in any L&amp;S LED Lighting, Sige Wire Ware or Kingslide Products between the 1st Februaru - 31st March.</p>
                        <a href="#" class="read-more hidden-xs hidden-sm">Read More</a>
                    </div>
                    <div class="col-xs-6 visible-xs-block visible-sm-block">
                        <a href="#" class="read-more">Read More</a>
                    </div>
                    <div class="col-xs-6 col-md-2 news-date">
                        10, April 2016
                    </div>
                </div>

                <?php } ?>
            </div>

            <div class="row button-list">
                <div class="col-lg-4 align-center row-btn"><a href="#" class="dark-btn btn-block">Viev L&amp;S Lighting Range</a></div>
                <div class="clearfix"></div>
                <div class="col-lg-4 align-center row-btn"><a href="#" class="dark-btn btn-block">Viev Side Wire Ware Range</a></div>
                <div class="clearfix"></div>
                <div class="col-lg-4 align-center row-btn"><a href="#" class="dark-btn btn-block">Viev Kingslide Range</a></div>
            </div>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>