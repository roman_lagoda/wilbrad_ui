<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-sm hidden-xs">

	        <?php output_sidebar('downloads', 2, 0); ?>
	        
		</div>
		
		<div class="main-content">

			<h1 class="main-header">Instructional &amp; Information Videos</h1>

			<?php
				$videos = array(
					array(
						'image' 	=> '1',
						'name' 		=> 'BLUM AVENTOS HK-XS Lift System - Assembly Video',
					),
					array(
						'image' 	=> '2',
						'name' 		=> 'BLUM BOXFIX E-L',
					),
					array(
						'image' 	=> '3',
						'name' 		=> 'BLUM Drilling Template for LEGRABOX Base Back',
					),
					array(
						'image' 	=> '4',
						'name' 		=> 'BLUM Drilling Template for TIP ON for TANDEMBOX',
					),
					array(
						'image' 	=> '5',
						'name' 		=> 'BLUM SERVO-DRIVE uno for bottom mount',
					),
					array(
						'image' 	=> '6',
						'name' 		=> 'BLUM SERVO-DRIVE uno for top mount',
					),
					array(
						'image' 	=> '7',
						'name' 		=> 'BLUM Universal Drilling Template - LEGRABOX Application',
					),
					array(
						'image' 	=> '8',
						'name' 		=> 'BLUM Universal Drilling Template',
					),
					array(
						'image' 	=> '9',
						'name' 		=> 'Soudal Adhesives Application Video',
					),
					array(
						'image' 	=> '10',
						'name' 		=> 'Accuride Pocket Door Installation',
					),
					array(
						'image' 	=> '11',
						'name' 		=> 'STRIPLOX Decorative Panel Installation',
					),
					array(
						'image' 	=> '12',
						'name' 		=> 'Blum spiral tech depth adjustment',
					),
				);
			?>

			<div class="row">
				<div class='clearfix'>

				<?php
					$index = -1;

					foreach($videos as $video)
					{
						$index++;

						// Every 4 items, add a new row container so videos with more text in their name don't break ones that are placed below it.
						if($index === 4)
						{
							$index = 0;
							echo "</div><div class='clearfix'>";
						}
				?>

					<div class="col-sm-3 grid-block instruction-video-grid-block">
						<a href="installation_videos_open.php" class="feature-link">
							<img class="img-responsive feature-image" src="img/install-videos/<?php echo $video['image']; ?>.jpg">
							<h2 class="feature-heading"><?php echo $video['name']; ?></h2>
							<div><span class="grid-orange-border"></span></div>
						</a>
					</div>

				<?php
						
					}

					// Make sure the row is closed off even if we don't reach an even amount of columns for this row
					if($index !== 0 && $index < 4) { echo "</div>"; }
				?>

			</div>

			<?php output_social_links(); ?>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>