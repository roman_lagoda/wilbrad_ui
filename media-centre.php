<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container">

	<div class="main-content">

		<img class="img-responsive media-centre-banner" src="img/media-centre-banner.jpg">

		<div class="row">

			<?php

				$videos = array(
					array(
						'image' 	=> 'wb_brochures-tile',
						'name' 		=> 'Brochures',
						'href'		=> 'flyers-brochures.php',
					),
					array(
						'image' 	=> 'wb_install-instruction-tile',
						'name' 		=> 'Install Instructions',
						'href'		=> 'installation-instructions.php',
					),
					array(
						'image' 	=> 'wb_msds-datasheets-tile',
						'name' 		=> 'Material Safety Datasheets',
						'href'		=> '#',
					),
					array(
						'image' 	=> 'wb_news-promotions-tile',
						'name' 		=> 'News &amp; Promotions',
						'href'		=> '#',
					),
					array(
						'image' 	=> 'wb_orderform-tile',
						'name' 		=> 'Order Forms',
						'href'		=> '#',
					),
					array(
						'image' 	=> 'wb_terms-tile',
						'name' 		=> 'Terms &amp; Conditions',
						'href'		=> '#',
					),
					array(
						'image' 	=> 'wb_video-instruction-tile',
						'name' 		=> 'Video Instructions',
						'href'		=> 'installation_videos.php',
					),
				);

				foreach($videos as $video) {
			?>

				<div class="col-sm-3 grid-block media-centre-grid-block">
					<a href="<?php echo $video['href']; ?>" class="feature-link">
						<img class="img-responsive feature-image" src="img/media-centre/<?php echo $video['image']; ?>.jpg">
						<h2 class="feature-heading"><?php echo $video['name']; ?></h2>
						<div><span class="grid-orange-border"></span></div>
					</a>
				</div>

			<?php } ?>

		</div>

	</div>

</div>

<?php
  // Output footer and we're done!
  output_footer();
?>