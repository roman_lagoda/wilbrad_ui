$(function()
{
	// Client logos slider
	$('.client-logos .row').slick(
	{
		prevArrow: 			$('.slider-previous'),
		nextArrow: 			$('.slider-next'),
		infinite: 			true,
		slidesToShow: 		5,
		slidesToScroll: 	1,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
				},
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 2,
				},
			}
		]
	});

	// Product Page slider

	$('.product-body .image-slider > div').slick(
	{
		prevArrow: 			$('.slider-prev'),
		nextArrow: 			$('.slider-next'),
		infinite: 			true,
		slidesToShow: 		3,
		slidesToScroll: 	1,
		vertical: 			true,
		verticalSwiping: 	true,
		centerMode: 		true,
        centerPadding: 		'90px',

	});

	$('.product-body .thumb-slider > div').slick(
	{
		prevArrow: 			$('.thumb-slider-prev'),
		nextArrow: 			$('.thumb-slider-next'),
		enterMode: 			true,
		centerPadding: 		'60px',
		slidesToShow:		 2,
		responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1
		      }
		    }
		  ]
	});


	$('.item-slide .image-slider > div').slick(
		{
			prevArrow: 			$('.slide-prev'),
			nextArrow: 			$('.slide-next'),
			enterMode: 			true,
			centerPadding: 		'60px',
			slidesToShow:		 2,
			responsive: [
		    {
		      breakpoint: 400,
		      settings: {
		        slidesToShow: 1,
		        centerPadding: '20px',
		      }
		    }
		  ]
		});

	$('.product-body .forgotton-box .product-slider > div').slick(
		{
			prevArrow: 			$('.product-slider-prev'),
			nextArrow: 			$('.product-slider-next'),
			infinite: 			true,
			slidesToShow: 		3,
			slidesToScroll: 	1,
			responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 2
		      }
		    },
		    {
		      breakpoint: 992,
		      settings: {
		        slidesToShow: 2
		      }
		    },
		    {
		      breakpoint: 786,
		      settings: {
		        slidesToShow: 1
		      }
		    }
		  ]
		});

	$('.product-body .forgotton-box .special-product-slider > div').slick(
		{
			prevArrow: 			$('.special-product-prev'),
			nextArrow: 			$('.special-product-next'),
			infinite: 			true,
			slidesToShow: 		3,
			slidesToScroll: 	1,
			responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 2
		      }
		    },
		    {
		      breakpoint: 992,
		      settings: {
		        slidesToShow: 2
		      }
		    },
		    {
		      breakpoint: 786,
		      settings: {
		        slidesToShow: 1
		      }
		    }
		  ]
		});

    $(document).ready(function() {

		$(".product-slider-container div img").click(function() {
			var image = $(this).attr('src');

			$('.feature-img .image-zoom-btn').attr("href", image);
			$('.feature-img .lightbox').attr("href", image);
			$('.feature-img .lightbox img').attr("src", image);
			$('.feature-img .lightbox-target img').attr("src", image);
		});


		$(".product-kit-list .product-list-row a").click(function(){
			var opt = $(this).attr("alt");

			$(this).parent().parent().toggleClass("active");
			$(".product-kit-list .option-" + opt).slideToggle();

    	});



    	$(".info-option .product-image .radio label").click(function() {

	    	$(this).parent().parent().parent().parent().find(".info-option").removeClass("bg-white");

	    	if(!$(this).parent().parent().parent().hasClass("bg-white")) {
				$(this).parent().parent().parent().addClass("bg-white");
	    	}
		});

		// $('.plus').click(function() {
		//     var sp = parseFloat($(this).parent().find('.span').text());
		//    $(this).parent().find('.span').text(sp + 1);
		// });
		//
		// $('.minus').click(function() {
		//     var sp = parseFloat($(this).parent().find('.span').text());
		//     if(sp > 0 ) {
		// 	    $(this).parent().find('.span').text(sp - 1);
		//     }
		// });

		$('.bottom-navbar .navbar-nav > li span').click(function() {
			$(this).toggleClass("glyphicon-triangle-bottom glyphicon-triangle-top")
			$(this).parent().next('.sub-menu').slideToggle();
		  $(this).parent().toggleClass("active");
		});

	});


	$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
		event.preventDefault();
		return $(this).ekkoLightbox({
			always_show_close: true
		});
	});






    $(document).ready(function () {
	    $('#zoom-in').click(function () {
	        $('#pic').width($('#pic').width()*1.2)
	        $('#pic').height($('#pic').height()*1.2)

	    })
	        $('#zoom-out').click(function () {
	        $('#pic').width($('#pic').width()/1.2)
	        $('#pic').height($('#pic').height()/1.2)

	    })

	    $('.carousel').carousel({
        	interval: 5000 ,
        	autoplay: false
    	})


	});



	// Initialise the custom radio and checkbox buttons
	$('input').iCheck(
	{
		checkboxClass: 		'icheckbox_square-orange',
		radioClass: 		'iradio_square-orange',
		increaseArea: 		'20%' // optional
	});

	// Limit the length of pagination links BEFORE initalisation
	if($.fn.DataTable != null)
	{
		$.fn.DataTable.ext.pager.numbers_length = 4;
	}

	// Initialise any standard datatables we need
	$('.dataTable').DataTable(
	{
		"searching": 		false,
		"dom": 				'<"top">rt<"bottom clearfix"flp><"clear">',
		"pagingType": 		"simple_numbers"
	});

	// Initialise datatables that have no pagination or filtering
	$('.dataTable-nofiltering').DataTable(
	{
		"searching": 		false,
		"paging": 			false,
		"info": 			false
	});

	// Initialise elements that need to be paginated/filtered/sorted
	$('#search-page').jplist(
	{
		itemsBox: '.search-results',
		itemPath: '.sub-category-item',
		panelPath: '.jplist-panel'
	});

	// Make sure all feature images are responsive
	var stretchImage = $("#header-feature-image").data('featureimg');

	if(stretchImage != null)
	{
		$("#header-feature-image").backstretch(stretchImage);
	}

	// todo: grab height from css of both elements and use that instead of hardcoded values
	// todo: code this better it's kinda shitty, but quick.

	// Search form
	var toggleSearch 				= false;
	var mobileSearchButton 			= $(".open-searchbar");
	var mobileSearchForm 			= $(".bottom-navbar .navbar-header");

	var originalHeight 				= 45;
	var expandedHeight 				= 90;
	var currentHeight 				= 0;

	// Orange line animation
	var gridTiles 					= $(".grid-block .feature-link");
	var animatedElement 			= ".grid-orange-border";
	var startSize 					= 20;
	var endSize 					= 100;

	// Expand the search bar when the button is pressed
	mobileSearchButton.on('click', function()
	{
		if(!toggleSearch)
		{
			currentHeight = expandedHeight;
		}
		else
		{
			currentHeight = originalHeight;
		}

		toggleSearch = !toggleSearch;

		mobileSearchForm.animate({ height: currentHeight+"px" }, 500);
	});

	// Animate the orange colored line when a grid tile is hovered
	gridTiles.hover(
		function()
		{
			$(this).find(animatedElement).stop().animate({ width: endSize+"%" }, 500);
		},
		function()
		{
			$(this).find(animatedElement).stop().animate({ width: startSize+"%" }, 500);
		}
	);

	// On the account payment page, show payment amount input if row is checked

	$('input').on('ifChanged', function(event){

		var selectedInvoice = $(this);
		var selectedPaymentAmount = $(this).closest("tr").find(".pay-amount");

		selectedInvoice.on('ifChecked', function (event){
			selectedPaymentAmount.removeClass("hidden-xs");
		});

		selectedInvoice.on('ifUnchecked', function (event){
			selectedPaymentAmount.addClass("hidden-xs");
		});

	});

	// On the checkout payment page toggle these two payment options when clicked
	if($(".payment-form").length)
	{
		$(".payment-form-container").show();
		$(".poli-info-container").hide();

		// Uses events from the iCheck plugin
		$(".payment-form #creditPayment").on('ifChecked', function(event){
			$(".payment-form-container").show();
			$(".poli-info-container").hide();
		});

		$(".payment-form #poliPayment").on('ifChecked', function(event){
			$(".payment-form-container").hide();
			$(".poli-info-container").show();
		});
	}

	// On larger screens, the footer elements are always displayed and we do not want the collapse to trigger.
	var responsiveFooterElements = $('.responsive-trigger .panel');

	var responsiveCollapse = function(e)
	{
		if($(window).width() > 992)
		{
			console.log("colapse cancelled");
			e.preventDefault();
		}
	};

	responsiveFooterElements.on('hide.bs.collapse', responsiveCollapse);
	responsiveFooterElements.on('show.bs.collapse', responsiveCollapse);

	// Interactive locations page with google map widget
	var mapContainer = $(".aus-map");
	var mapButtons = $(".aus-map button");
	var locationContainer = $(".map-locations");

	var gmapLocations = {
		"melbourne" : {
			lat: -37.7461828,
			lng: 145.02531999999997,
			content: "<strong>Wilson & Bradley Melbourne</strong><br>94 Bell St Preston, 3072 Victoria, Australia<br>"
		},
		"brisbane" : {
			lat: -27.4737436,
			lng: 153.11297890000003,
			content: "<strong>Wilson & Bradley Queensland</strong><br>2/60 Enterprise Place Tingalpa QLD 4173<br>"
		},
		"sydney" : {
			lat: -33.823244,
			lng: 150.94363299999998,
			content: "<strong>Wilson & Bradley Sydney</strong><br>Unit4, 3 Basalt Road Greystanes NSW 2145<br>"
		},
		"perth" : {
			lat: -31.7915046,
			lng: 115.84148989999994,
			content: "<strong>Wilson & Bradley Perth</strong><br>43 Prosperity Ave Wangara WA 6065<br>"
		},
		"adelaide" : {
			lat: -34.9855106,
			lng: 138.5695624,
			content: "<strong>Wilson & Bradley Adelaide</strong><br>41 Weaver Street Edwardstown SA 5039<br>"
		},
		"hobart" : {
			lat: 0,
			lng: 0,
			content: ""
		},
	};

	if(mapContainer.length)
	{
		init_google_map(gmapLocations.melbourne);

		mapButtons.on("click", function(e)
		{
			var thisO = $(this);

			init_google_map(gmapLocations[thisO.prop("id")]);

			$('.aus-map button.active').removeClass('active');
			thisO.addClass('active');

			$(".map-locations .active").removeClass('active');
			$(".map-locations ." + thisO.prop("id") + "-location").addClass("active");
		});
	}

});

// Put together from: http://embedgooglemaps.com/en/
function init_google_map(dataset)
{
	if(dataset.content != "")
	{
		var myOptions = {zoom:13,center:new google.maps.LatLng(dataset.lat, dataset.lng),mapTypeId: google.maps.MapTypeId.ROADMAP};
		map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
		marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(dataset.lat, dataset.lng)});
		infowindow = new google.maps.InfoWindow({content:dataset.content});
		google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});
		infowindow.open(map,marker);
	}
}

/*
$(window).resize(function()
{

});
*/