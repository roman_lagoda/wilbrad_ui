<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar">

			<?php output_sidebar('lists', 1); ?>
	        
		</div>
		
		<div class="main-content">

			<h1 class="main-header sub-header">Application Forms</h1>
			<p>Wilson &amp; Bradley Warranty Cards Listing - Click on a documents name to view it</p>
            <p>Click <a href="#">here</a> to download the Adobe Acrobat viewer for free.</p>
				
			<table class="styled-table striped-table application-forms-table" cellspacing="0" width="100%">
                <tbody>
                    <?php for($i=0; $i < 10; $i++) { ?>
                        <tr>
                            <td><a href="#"><img src="img/icons/pdf_icon_small.png"> Application form entry</a></td>
                            <td><span class="bold">1.2mb</span></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

            <h3>Apply for a Web Account</h3>
            <a href="#" class="dark-btn">Account Customer</a>
            <a href="#" class="dark-btn">Non-Account Customer</a>

		    <?php output_social_links(); ?>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>