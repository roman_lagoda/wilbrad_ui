<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  $results_amount = 100;
?>

<div class="container">

	<div id="search-page" class="main-content">

		<h1 class="main-header sub-header">Search</h1>
        <h2 class="smaller-heading">Results in Shop</h2>
        <p>Your search for <span class="bold">blum</span> returned <span class="bold"><?php Echo $results_amount; ?></span> results</p>

        <!-- PRODUCT SEARCH RESULTS -->
        <div class="search-results clearfix">
            <?php for($i=0; $i < $results_amount; $i++) { ?>
                <div class="col-xs-6 col-lg-4 sub-category-item">
                    <div class="sub-category-container clearfix">
                        <a href="step5.php"><img class="img-responsive feature-image" src="img/category-list-individual/SOUFLEX_1.jpg"></a>
                        <div class="product-heading">                            <h2><a href="step5.php">Search Result #<?php echo $i; ?></a></h2>                            <div><span class="grid-orange-border"></span></div>                        </div>
                        <!-- <p>Blum TANDEMBOX Intivo with TIP-ON M Height 83mm drawer 270mm in Silk White 30KG</p> -->
                        <div class="col-sm-6 product-code">SOULFELX</div>
                        <div class="col-sm-6 product-qty">                            <label>Qty</label>                            <input type="number" value="1">                        </div>
                        <div class="col-sm-6 add-to-favourites hidden-xs"><a href="#" class="btn-block">Add to favourites</a></div>
                        <div class="col-sm-6 add-to-cart"><a href="#" class="btn-block">Add to cart</a></div>
                    </div>
                </div>
            <?php } ?>
        </div>

        <!-- PRODUCT SEARCH RESULTS CONTROL PANEL -->
        <div class="jplist-panel clearfix">

            <!-- PAGINATION OUTPUT -->
            <div class="col-sm-6 col-md-8">
                <ul
                    class="pagination pull-left jplist-pagination"
                    data-control-type="boot-pagination"
                    data-control-name="paging"
                    data-control-action="paging"
                    data-range="3"
                    data-mode="google-like">
                </ul>
            </div>

            <!-- FILTERING -->
            <div class="col-sm-6 col-md-4">
                <div class="row">
                    <div class="col-xs-6">

                        <!-- pagination info label -->
                        <div
                            class="pull-left jplist-pagination-info"
                            data-type="<strong>Page {current} of {pages}</strong><br/> <small>{start} - {end} of {all}</small>"
                            data-control-type="pagination-info"
                            data-control-name="paging"
                            data-control-action="paging"></div>

                    </div>
                    <div class="col-xs-6">

                        <!-- items per page dropdown -->
                        <div
                            class="dropdown jplist-items-per-page"
                            data-control-type="boot-items-per-page-dropdown"
                            data-control-name="paging"
                            data-control-action="paging">

                            <button
                                class="btn btn-block orange-btn dropdown-toggle"
                                type="button"
                                data-toggle="dropdown"
                                id="dropdown-menu-1"
                                aria-expanded="true">
                                <span data-type="selected-text">Items per Page</span>
                                <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdown-menu-1">

                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="#" data-number="10" data-default="true">10 per page</a>
                                </li>

                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="#" data-number="25">25 per page</a>
                                </li>

                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="#" data-number="50">50 per page</a>
                                </li>

                                <li role="presentation" class="divider"></li>

                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="#" data-number="all">View All</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <!-- TEXT SEARCH RESULTS CONTROL PANEL -->
        <h2 class="extra-results-header">Results in Pages</h2>
        <p>Your search for <span class="bold">blum</span> returned <span class="bold"><?php Echo $results_amount; ?></span> results</p>

        <ul class="text-search-results">
            <?php for($i=0; $i < 5; $i++) { ?>

            <li>
                <h2>Search result headline</h2>
                <p class="result-url">/path/to/link.aspx</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ante est, vulputate in urna eget, vestibulum hendrerit sem. Pellentesque auctor dictum semper. Etiam at aliquam nisl, vel tempus tellus. Fusce fermentum faucibus erat vel lacinia. Nam tincidunt a lacus pulvinar facilisis. Ut sit amet diam in ex bibendum tempus.</p>
            </li>

            <?php } ?>
        </ul>

	</div>

</div>

<?php
  // Output footer and we're done!
  output_footer();
?>