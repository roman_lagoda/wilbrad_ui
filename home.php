<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
      
      <!-- Call to action banner -->
      <div class="action-banner container">
        <a href="#"><img class="img-responsive" src="img/call-to-action.jpg"></a>
      </div>

      <!-- Product listing grid -->
      <div class="home-grid container">
        <div class="row">
          
          <div class="col-sm-6 col-md-4 grid-block home-grid-block">
            <a href="#" class="feature-link">
              <img class="img-responsive feature-image" src="img/home-grid/home-grid-specials.jpg">
              <h2 class="feature-heading">Specials</h2>
              <div><span class="grid-orange-border"></span></div>
            </a>
          </div>

          <div class="col-sm-6 col-md-4 grid-block home-grid-block">
            <a href="#" class="feature-link">
              <img class="img-responsive feature-image" src="img/home-grid/home-grid-perfect-motion.jpg">
              <h2 class="feature-heading">Blum Products - Perfect Motion</h2>
              <div><span class="grid-orange-border"></span></div>
            </a>
          </div>

          <div class="col-sm-6 col-md-4 grid-block home-grid-block">
            <a href="#" class="feature-link">
              <img class="img-responsive feature-image" src="img/home-grid/home-grid-all-spaces.jpg">
              <h2 class="feature-heading">Cabinet Hardware - For All Spaces</h2>
              <div><span class="grid-orange-border"></span></div>
            </a>
          </div>

          <div class="col-sm-6 col-md-4 grid-block home-grid-block">
            <a href="#" class="feature-link">
              <img class="img-responsive feature-image" src="img/home-grid/home-grid-complete-sets.jpg">
              <h2 class="feature-heading">Kits - Order Complete Sets</h2>
              <div><span class="grid-orange-border"></span></div>
            </a>
          </div>

          <div class="col-sm-6 col-md-4 grid-block home-grid-block">
            <a href="#" class="feature-link">
              <img class="img-responsive feature-image" src="img/home-grid/home-grid-aluminium-doors.jpg">
              <h2 class="feature-heading">Aluminium Doors</h2>
              <div><span class="grid-orange-border"></span></div>
            </a>
          </div>

          <div class="col-sm-6 col-md-4 grid-block home-grid-block">
            <a href="#" class="feature-link">
              <img class="img-responsive feature-image" src="img/home-grid/home-grid-roller-shutters.jpg">
              <h2 class="feature-heading">Roller Shutters</h2>
              <div><span class="grid-orange-border"></span></div>
            </a>
          </div>

          <div class="col-sm-6 col-md-4 grid-block home-grid-block">
            <a href="#" class="feature-link">
              <img class="img-responsive feature-image" src="img/home-grid/home-grid-virtual-showroom.jpg">
              <h2 class="feature-heading">Virtual Showroom</h2>
              <div><span class="grid-orange-border"></span></div>
            </a>
          </div>

          <div class="col-sm-6 col-md-4 grid-block home-grid-block">
            <a href="#" class="feature-link">
              <img class="img-responsive feature-image" src="img/home-grid/home-grid-product-config.jpg">
              <h2 class="feature-heading">Blum Online Product Configurator</h2>
              <div><span class="grid-orange-border"></span></div>
            </a>
          </div>

          <div class="col-sm-6 col-md-4 grid-block home-grid-block">
            <a href="#" class="feature-link">
              <img class="img-responsive feature-image" src="img/home-grid/home-grid-media-centre.jpg">
              <h2 class="feature-heading">Media Centre</h2>
              <div><span class="grid-orange-border"></span></div>
            </a>
          </div>

        </div>
      </div>
      
      <!-- Icon Links -->
      <div class="icon-links dark-row-full-width">
        <div class="container">
          <div class="row">
            
            <div class="icon-link col-sm-6 col-md-3">
              <a href="#">
                <div class="img-uniform-gap"><img src="img/icons/icon-product-config.png"></div>
                <h2>Online Product Configurator</h2>
                <p>Chose the right product for every room in your home.</p>
              </a>
            </div>

            <div class="icon-link col-sm-6 col-md-3">
              <a href="#">
                <div class="img-uniform-gap"><img src="img/icons/icon-contact.png"></div>
                <h2>Contact and About Us</h2>
                <p>Contact details and general information about Wilson &amp; Bradley.</p>
              </a>
            </div>

            <div class="icon-link col-sm-6 col-md-3">
              <a href="#">
                <div class="img-uniform-gap"><img src="img/icons/icon-faq.png"></div>
                <h2>FAQ</h2>
                <p>A list of frequently asked questions regarding products and trading terms.</p>
              </a>
            </div>

            <div class="icon-link col-sm-6 col-md-3">
              <a href="#">
                <div class="img-uniform-gap"><img src="img/icons/icon-media.png"></div>
                <h2>Media Centre</h2>
                <p>View all the installation videos, brochures and accounts documents.</p>
              </a>
            </div>

          </div>
        </div>
      </div>

      <!-- Client logos -->
      <div class="client-logos light-row-full-width">
        <div class="container">
          
          <a href="#" class="slider-previous"><img src="img/slider-left-prev.png"></a>

          <div class="row">
            
            <div class="logo-column col-xs-6 col-sm-4 col-md-5ths">
              <a href="#"><img class="img-responsive" src="img/client-logos/client-wesco.png"></a>
            </div>

            <div class="logo-column col-xs-6 col-sm-4 col-md-5ths">
              <a href="#"><img class="img-responsive" src="img/client-logos/client-blum.png"></a>
            </div>

            <div class="logo-column col-xs-6 col-sm-4 col-md-5ths">
              <a href="#"><img class="img-responsive" src="img/client-logos/client-sige.png"></a>
            </div>

            <div class="logo-column col-xs-6 col-sm-4 col-md-5ths">
              <a href="#"><img class="img-responsive" src="img/client-logos/client-striplox.png"></a>
            </div>

            <div class="logo-column col-xs-6 col-sm-4 col-md-5ths">
              <a href="#"><img class="img-responsive" src="img/client-logos/client-kingslide.png"></a>
            </div>

            <div class="logo-column col-xs-6 col-sm-4 col-md-5ths">
              <a href="#"><img class="img-responsive" src="img/client-logos/client-wesco.png"></a>
            </div>

            <div class="logo-column col-xs-6 col-sm-4 col-md-5ths">
              <a href="#"><img class="img-responsive" src="img/client-logos/client-blum.png"></a>
            </div>

            <div class="logo-column col-xs-6 col-sm-4 col-md-5ths">
              <a href="#"><img class="img-responsive" src="img/client-logos/client-sige.png"></a>
            </div>

            <div class="logo-column col-xs-6 col-sm-4 col-md-5ths">
              <a href="#"><img class="img-responsive" src="img/client-logos/client-striplox.png"></a>
            </div>

            <div class="logo-column col-xs-6 col-sm-4 col-md-5ths">
              <a href="#"><img class="img-responsive" src="img/client-logos/client-kingslide.png"></a>
            </div>

          </div>

          <a href="#" class="slider-next"><img src="img/slider-right-next.png"></a>

        </div>
      </div>

<?php
  // Output footer and we're done!
  output_footer();
?>