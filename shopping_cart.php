<?php
  // Grab template functions
require_once('inc/template.php');

  // Output header
output_header();

  // Page content below:
?>
<div class="container">
    <div class="main-content payment-content shopping-cart-page">

        <h1 class="main-header">Shopping Cart</h1>

        <a href="#">Go To My Favourites</a>

        <!-- Cart items -->
        <div class="clearfix hidden-xs product-list-header">
            <div class="col-sm-1 col-md-1">&nbsp;</div>
            <div class="col-sm-5 col-md-6 align-left">Item Name</div>
            <div class="col-sm-1 col-md-1">Qty</div>
            <div class="col-sm-2 col-md-2">Unit Price<small class="grey-text">ext GST</small></div>
            <div class="col-sm-2 col-md-1">Line Price<small class="grey-text">ext GST</small></div>
            <div class="col-sm-1 col-md-1">&nbsp;</div>
        </div>

        <div class="visible-xs-block mobile-product-list-header"></div>

        <?php for($i=0; $i<4; $i++) { ?>

            <div class="clearfix product-list-row checkout-product-list-row">
                <div class="col-xs-6 col-sm-1 col-md-1 product-image"><img src="img/product-kits/WB8043_1.jpg" class="img-responsive" /></div>
                <div class="col-xs-6 col-sm-5 col-md-6 product-name align-left">Blum TANDEMBOX Intivo D Height 211MM drawer BOXCOVER- integrated BLUMOTION 500MM in Stainless Steel 65KG</div>

                <div class="hidden-xs col-sm-1 col-md-1 product-qty"><input type="number" min="0" value="1" class="text-center"></div>
                <div class="hidden-xs col-sm-2 col-md-2 product-price">$240.8828</div>
                <div class="hidden-xs col-sm-2 col-md-1 product-total" style="padding-right:0">$240.8828</div>

                <div class="col-xs-12 visible-xs-block price-display">
                    <div class="col-xs-4 col-sm-1 col-md-1 product-qty">
                        <span>Qty</span>
                        <input type="number" min="0" value="1" class="text-center">
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-2 product-price">
                        <span>Unit Price</span>
                        $240.8828
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-1 product-total" style="padding-right:0">
                        <span>Line Price</span>
                        $240.8828
                    </div>
                </div>

                <div class="col-xs-offset-10 col-xs-2 col-sm-offset-0 col-sm-1 remove-from-cart">
                    <a href="#" class="text-right">
                        <img src="img/icons/remove_icon.png">
                    </a>
                </div>

            </div>

        <?php } ?>
        <!-- Price information -->
        <div class="row">
            <div class="col-sm-8 hidden-xs">                <a href="#" class="save-to-favourites">Save Cart to Favourite</a>
                <a href="#" class="empty-cart">Empty Cart</a>                <p> Please note all credit card payments will incur a 2% surcharge </p>            </div>            <div class="col-sm-4">

    					<!-- Mobile Price display -->

    					<div class="row price-rows price-rows-nopadding hidden-md hidden-lg">



    						<div class="col-xs-12 text-right">

    							<span class="col-xs-8">Sub Total</span>

    							<span class="bold">$240.8828</span>

    						</div>



    						<div class="col-xs-12 text-right">

    							<span class="col-xs-8">Shipping</span>

    							<span class="bold align-right">$19.80</span>

    						</div>



    						<div class="col-xs-12 text-right">

    							<span class="col-xs-8">Total</span>

    							<span class="bold align-right">

    								$288.62<br>

    								<small class="no-bold">(Including GST)</small>

    							</span>

    						</div>



    					</div>

    					<!-- Desktop Price display -->

    					<div class="price-rows hidden-sm hidden-xs">



    						<div class="col-md-8 align-right">Sub Total</div>

    						<div class="col-md-4 bold align-right price-value">$240.8828</div>



    						<div class="col-md-8 align-right">Shipping</div>

    						<div class="col-md-4 bold align-right price-value">$19.80</div>


    						<div class="col-md-8 bold align-right">Total</div>

    						<div class="col-md-4 bold align-right price-value">$288.62<br><small class="no-bold">(Including GST)</small></div>



    					</div>

            </div>
        </div>

        <!-- Final order details -->
        <div class="row finalise-order">
          <div class="col-sm-7 visible-xs">            <a href="#" class="save-to-favourites">Save Cart to Favourite</a>            <a href="#" class="empty-cart">Empty Cart</a>            <p> Please note all credit card payments will incur a 2% surcharge </p>          </div>
            <div class="col-sm-offset-6 col-md-offset-7 col-md-5">
                <form class="styled-form row">
                    <div class="col-xs-8">
                        <div class="form-group">
                            <input type="text" class="form-control" id="exampleInput1" placeholder="Promotional Code">
                        </div>
                        <p>Please enter a Promo Code if you have one</p>
                    </div>
                    <div class="col-xs-4 text-right">
                        <button type="submit" class="dark-btn btn-default">Apply</button>
                    </div>                </form>
            </div>

            <div class="col-xs-12 col-sm-offset-6 col-sm-6 col-md-offset-9 col-md-3">
                <a href="#forgotton-box" href="step13.php" class="lightbox orange-fill btn-block align-center finalise-order-btn">Checkout</a>
            </div>

        </div>

        <br>
        <br>
        <a href="#" data-toggle="modal" data-target="#myModal">< Back to Shopping</a>

        <?php output_social_links(); ?>

    </div>
</div>

<!-- Customised Modal -->
<div class="modal fade custom-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Continue shopping?</h4>
      </div>
      <div class="modal-body">
        <p>
            Would you like to continue shopping?
        </p>
      </div>
      <div class="modal-footer">
        <div class="row">
            <div class="col-md-6"><button type="button" class="dark-btn btn-block" data-dismiss="modal">Back to cart</button></div>
            <div class="col-md-6"><a href="step8-1.php" class="orange-fill btn-block align-center">Continue Shopping</a></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Have you forgotton --><div class="col-xs-12 col-sm-12 col-md-12 forgot">  <div class="lightbox-target" id="forgotton-box">    <?php    $products = array(      array(        'name' 		=> 'Soudal Fix All X-TREME Power 290ml',        'image' 	=> 'SOUXTR_1.jpg',        'sku' 		=> 'SOUXTR',      ),      array(        'name' 		=> 'Soudal Heavy Duty Cartridge Gun',        'image' 	=> 'SOUHDCG_1.jpg',        'sku' 		=> 'SOUHT_1',      ),      array(        'name' 		=> 'Twisty Swivle Nozzle for Soudal',        'image' 	=> 'SOUSNO_1.jpg',        'sku' 		=> 'SOUSNO',      )    );  ?>    <div class="col-xs-12 col-sm-8 col-md-8 forgotton-box clearfix">      <div>        <div class="clearfix">          <div class="col-md-12">          <h3>Have you forgotten</h3>          <h4>Product you have previously purchsed</h4>          <a class="lightbox-close" href="#"></a>          </div>        </div>         <div class="row clearfix">          <div class="product-slider clearfix">            <button class="slider-btn product-slider-prev"></button>            <div class="products-item col-xs-12">              <?php foreach($products as $v=>$product) { ?>              <div class="col-xs-12 col-sm-6 col-md-4 clearfix">                <a href="step5.php">                  <img class="img-responsive feature-image" src="img/category-list-individual/<?php echo $product['image']; ?>">                </a>                <div class="col-xs-12 product-box">                  <div class="row">                    <div class="col-xs-9 col-sm-9 col-md-9 product-heading">                      <h2><a href="step5.php"><?php echo $product['name']; ?></a></h2>                      <div><span class="grid-orange-border"></span></div>                    </div>                    <div class=" col-xs-3 col-sm-3 col-md-3 compact-favourites"><img src="img/icons/favourite_star.png"></div>                  </div>                  <div class="row">                    <div class="col-xs-9 col-sm-9 col-md-7 col-lg-5 product-code"><?php echo $product['sku']; ?></div>                    <div class="col-xs-4 col-sm-3 col-md-4 col-lg-4 product-qty hidden-xs hidden-sm hidden-md">                      <span class="span">1</span>                      <span class="button plus">+</span>                      <span class="button minus">-</span>                    </div>                    <div class="col-xs-3 col-sm-3 col-md-5  col-lg-3 compact-add-to-cart">                      <a href="#"><img src="img/icons/shopping_cart_orange.png"></a>                    </div>                  </div>                  <div class="row hidden-lg">                    <div class="col-xs-4 col-sm-3 col-md-4 product-qty">                      <span class="span">1</span>                      <span class="button plus">+</span>                      <span class="button minus">-</span>                    </div>                  </div>                </div>              </div>              <?php } ?>            </div>            <button class="slider-btn product-slider-next"></button>          </div>         </div>      <div class="row clearfix">        <div class="specials col-md-12">          <h4 class="col-sm-12 col-md-2">Specials</h4>          <p class="col-sm-12 col-md-10">Enter promo code SPECIALS at checkout to receive a 10% discounts on these products</p>        </div>        <div class="row special-product-slider clearfix">          <button class="slider-btn special-product-prev"></button>          <div class=" products-item col-xs-12 col-sm-12 col-md-12">            <?php foreach($products as $v=>$product) { ?>            <!-- Related Product Box -->            <div class="col-xs-12 col-sm-6 col-md-4 clearfix">              <a href="step5.php"><img class="img-responsive feature-image" src="img/category-list-individual/<?php echo $product['image']; ?>"></a>              <div class="col-xs-12 col-sm-12 col-md-12 product-box">                <div class="row">                  <div class="col-xs-9 col-sm-9 col-md-9 product-heading">                    <h2><a href="step5.php"><?php echo $product['name']; ?></a></h2>                    <div><span class="grid-orange-border"></span></div>                  </div>                  <div class="col-xs-3 col-sm-3 col-md-3 compact-favourites"><img src="img/icons/favourite_star.png"></div>                </div>                <div class="row">                    <div class="col-xs-9 col-sm-9 col-md-7 col-lg-5 product-code"><?php echo $product['sku']; ?></div>                    <div class="col-xs-4 col-sm-3 col-md-4 col-lg-4 product-qty hidden-xs hidden-sm hidden-md">                      <span class="span">1</span>                      <span class="button plus">+</span>                      <span class="button minus">-</span>                    </div>                    <div class="col-xs-3 col-sm-3 col-md-5  col-lg-3 compact-add-to-cart">                      <a href="#"><img src="img/icons/shopping_cart_orange.png"></a>                    </div>                  </div>                  <div class="row hidden-lg">                    <div class="col-xs-4 col-sm-3 col-md-4 product-qty">                      <span class="span">1</span>                      <span class="button plus">+</span>                      <span class="button minus">-</span>                    </div>                  </div>              </div>            </div>            <?php } ?>          </div>          <button class="slider-btn special-product-next"></button>        </div>      </div>      <div class="row clearfix">         <div class="checkbox col-xs-12 col-sm-12 col-md-4 radio show">                      <label>                          <input type="checkbox" value="same_as_postal" checked>                          dont show this again                      </label>                  </div>        <div class="col-xs-12 col-sm-12 col-md-8">          <div class="col-xs-12 col-sm-12 col-md-6 add-to-favs">            <a href="#">Review Shopping Cart</a>          </div>          <div class="col-xs-12 col-sm-12 col-md-6 add-to-cart">            <a href="step10.php" class="orange-fill">Proceed</a>          </div>        </div>      </div>      </div>    </div>  </div></div>
<?php
  // Output footer and we're done!
output_footer();
?>
