<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-xs hidden-sm">

	        <ul>
				<li><a href="#">Specials</a></li>
				<li><a href="#">Blum Products - Perfecting Motion</a></li>
				<li>
					<a href="#" class="active">Cabinet Hardware - For All Spaces</a>

					<ul>
				        <li><a href="#">Abrasives</a></li>
						<li>
							<a href="#" class="active">Adhesives</a>

							<ul>
								<li><a href="#">Bostik Adhesives</a></li>
								<li><a href="#">Tensorgrip Canister Adhesives</a></li>
								<li><a href="#">TAC &amp; Wilson &amp; Bradley Adhesives</a></li>
								<li><a href="#">Titebond Adhesives</a></li>
								<li><a href="#">Consolidated Veneers Adhesives</a></li>
								<li><a href="#" class="active">Soudal Adhesives</a></li>
								<li><a href="#">H.B. FULLER ADHESIVES</a></li>
							</ul>

						</li>
						<li><a href="#">Bathroom Partition Hardware</a></li>
						<li><a href="#">Bolts, Latches &amp; Brackets</a></li>
						<li><a href="#">Buffers</a></li>
						<li><a href="#">Castors &amp; Guides</a></li>
						<li><a href="#">Catches</a></li>
						<li><a href="#">Connector Fittings &amp; Clips</a></li>
						<li><a href="#">Consolidated Veneers &amp; T-Section</a></li>
						<li><a href="#">Cover Caps &amp; Closures</a></li>
						<li><a href="#">Cutlery Trays, Dividers &amp; Acc</a></li>
						<li><a href="#">Drawer Systems &amp; Runners</a></li>
						<li><a href="#">Fastening Systems</a></li>
						<li><a href="#">Gun Brads and Staples</a></li>
						<li><a href="#">Handles</a></li>
						<li><a href="#">Hinges</a></li>
						<li><a href="#">Hooks</a></li>
						<li><a href="#">Ironing Board &amp; Accessories</a></li>
						<li><a href="#">Kitchen Bins</a></li>
						<li><a href="#">Kitchen Fittings</a></li>
						<li><a href="#">LED Lighting- L &amp; S</a></li>
						<li><a href="#">LED Lights- Battery Operated</a></li>
						<li><a href="#">LED Lighting- Strip Lighting</a></li>
						<li><a href="#">Legs</a></li>
						<li><a href="#">Locks</a></li>
						<li><a href="#">Office Fittings</a></li>
						<li><a href="#">Safety Products</a></li>
						<li><a href="#">Screws</a></li>
						<li><a href="#">Screwdrivers &amp; Driver Bits</a></li>
						<li><a href="#">Shelving</a></li>
						<li><a href="#">Stays</a></li>
						<li><a href="#">STRIPLOX Products</a></li>
						<li><a href="#">Thingamejig Precision Tools</a></li>
						<li><a href="#">Tools</a></li>
						<li><a href="#">Towel Rails</a></li>
						<li><a href="#">Tracks Folding &amp; Sliding</a></li>
						<li><a href="#">Vents</a></li>
						<li><a href="#">Vinyl Wrapped Drawers</a></li>
						<li><a href="#">Wardrobe Systems &amp; Fittings</a></li>
						<li><a href="#">Wire Baskets</a></li>
						<li><a href="#">Wire Ware Range-Sige Infinity Plus</a></li>
						<li><a href="#">Wire Ware &amp; Corner Solutions</a></li>
						<li><a href="#">Zipbolts</a></li>
						<li><a href="#">Miscellaneous</a></li>
					</ul>
				</li>
				<li><a href="#">Aluminium Doors &amp; Rollershutters</a></li>
			</ul>

			<h1>Kits</h1>

	        <ul>
		        <li><a href="#">TANDEMBOX Intivo</a></li>
		        <li><a href="#">TANDEMBOX Antaro</a></li>
		        <li><a href="#">LEGRABOX Pure</a></li>
		        <li><a href="#">TANDEMBOX Antaro &amp; Intivo with TIP-ON</a></li>
		        <li><a href="#">SERVO-DRIVE bank</a></li>
		        <li><a href="#">TANDEMBOX antaro Drip Trays</a></li>
		        <li><a href="#">TANDEMBOX intivo Drip Trays</a></li>
		        <li><a href="#">TANDEMBOX antaro SPACE TOWER</a></li>
		        <li><a href="#">TANDEMBOX intivo SPACE TOWER</a></li>
		        <li><a href="#">TANDEMBOX intivo Standard Drawers with Wesco Pull Boy Bin</a></li>
		        <li><a href="#">TANDEMBOX antaro Standard Drawers with Wesco Pull Boy Bin</a></li>
		        <li><a href="#">TANDEMBOX intivo Standard Drawers with ORGA-LINE</a></li>
		        <li><a href="#">TANDEMBOX antaro Standard Drawers with ORGA-LINE</a></li>
		        <li><a href="#">TANDEMBOX intivo trade pack kitcodes</a></li>
		        <li><a href="#">TANDEMBOX antaro trade pack kitcodes</a></li>
		        <li><a href="#">L &amp; S LED Lighting</a></li>
		        <li><a href="#">LEGRABOX Pure Standard Drawers with Wesco Pull Boy Bins</a></li>
		    </ul>
	        
		</div>
		
		<div class="main-content clearfix">

			<img class="img-responsive category-banner" src="img/promo_banner_two.jpg">

			<h1 class="main-header">Soudal Adhesives</h1>
			
			<div class="sub-category-list col-xs-12 col-md-12">
				
				<?php
					$products = array(
						array(
							'name' 		=> 'Soudal Fix All Flexi 290ml',
							'image' 	=> 'SOUFLEX_1.jpg',
							'sku' 		=> 'SOULFELX',
						),
						array(
							'name' 		=> 'Soudal Fix All High Tack Super Strong Bonding Sealant 290ml',
							'image' 	=> 'SOUHT_1.jpg',
							'sku' 		=> 'SOUHT',
						),
						array(
							'name' 		=> 'Soudal Fix All Crystal 290ml Clear',
							'image' 	=> 'SOUCC_1.jpg',
							'sku' 		=> 'SOUCC',
						),
						array(
							'name' 		=> 'Soudal Fix All Turbo 290ml White',
							'image' 	=> 'SOUTUR_1.jpg',
							'sku' 		=> 'SOUTUR',
						),
						array(
							'name' 		=> 'Soudal Fix All X-TREME Power 290ml',
							'image' 	=> 'SOUXTR_1.jpg',
							'sku' 		=> 'SOUXTR',
						),
						array(
							'name' 		=> 'Soudal Heavy Duty Cartridge Gun',
							'image' 	=> 'SOUHDCG_1.jpg',
							'sku' 		=> 'SOUHT_1',
						),
						array(
							'name' 		=> 'Twisty Swivle Nozzle for Soudal Silicone adhesives Pack of 5',
							'image' 	=> 'SOUSNO_1.jpg',
							'sku' 		=> 'SOUSNO',
						),
						array(
							'name' 		=> 'Soudal Swipex Super Cleaning Wipes 100pk',
							'image' 	=> 'SOUSWW_1.jpg',
							'sku' 		=> 'SOUSWW',
						),
					);
				?>

				<?php foreach($products as $product) { ?>

					<!-- Category Box -->
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 sub-category-item">
						<div class="sub-category-container clearfix">

							<a href="step5.php"><img class="img-responsive feature-image" src="img/category-list-individual/<?php echo $product['image']; ?>"></a>

							<div class="product-heading">
								<h2><a href="step5.php"><?php echo $product['name']; ?></a></h2>
								<div><span class="grid-orange-border"></span></div>	
							</div>

							<!-- <p>Blum TANDEMBOX Intivo with TIP-ON M Height 83mm drawer 270mm in Silk White 30KG</p> -->

							<div class="col-sm-6 col-md-6 product-code"><?php echo $product['sku']; ?></div>

							<div class="col-sm-6 col-md-6 product-qty">
								<label>Qty</label>
								<input type="number" value="1">
							</div>

							<div class="col-sm-6 col-md-6 add-to-favourites hidden-xs"><a href="#" class="btn-block">Add to favourites</a></div>
							<div class="col-sm-6 col-md-6 add-to-cart"><a href="#" class="btn-block">Add to cart</a></div>

						</div>
					</div>

				<?php } ?>

			</div>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>