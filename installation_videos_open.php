<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-sm hidden-xs">

	        <?php output_sidebar('downloads', 2, 0); ?>
	        
		</div>
		
		<div class="main-content">

			<iframe width="100%" height="400" src="https://www.youtube.com/embed/uNlNTpWKiY8" frameborder="0" allowfullscreen></iframe>

			<?php
				$videos = array(
					array(
						'image' 	=> '1',
						'name' 		=> 'BLUM AVENTOS HK-XS Lift System - Assembly Video',
					),
					array(
						'image' 	=> '2',
						'name' 		=> 'BLUM BOXFIX E-L',
					),
					array(
						'image' 	=> '3',
						'name' 		=> 'BLUM Drilling Template for LEGRABOX Base Back',
					),
					array(
						'image' 	=> '4',
						'name' 		=> 'BLUM Drilling Template for TIP ON for TANDEMBOX',
					),
				);
			?>

			<div class="row" style="margin-top:40px">
				<div class='clearfix'>

				<?php
					$index = -1;

					foreach($videos as $video)
					{
						$index++;

						// Every 4 items, add a new row container so videos with more text in their name don't break ones that are placed below it.
						if($index === 4)
						{
							$index = 0;
							echo "</div><div class='clearfix'>";
						}
				?>

					<div class="col-sm-3 grid-block instruction-video-grid-block">
						<a href="installation_videos_open.php" class="feature-link">
							<img class="img-responsive feature-image" src="img/install-videos/<?php echo $video['image']; ?>.jpg">
							<h2 class="feature-heading"><?php echo $video['name']; ?></h2>
							<div><span class="grid-orange-border"></span></div>
						</a>
					</div>

				<?php
						
					}

					// Make sure the row is closed off even if we don't reach an even amount of columns for this row
					if($index !== 0 && $index < 4) { echo "</div>"; }
				?>

			</div>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>