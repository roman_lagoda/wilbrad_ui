<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container">

	<div class="main-content">

		<div class="panel-group login-panels" id="loginAccordian" role="tablist" aria-multiselectable="true">
		  <div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="loginHeadingOne">
		      <h4 class="panel-title">
		        <a role="button" data-toggle="collapse" data-parent="#loginAccordian" href="#loginCollapseOne" aria-expanded="true" aria-controls="loginCollapseOne">
		          Login for account customer
		        </a>
		      </h4>
		    </div>
		    <div id="loginCollapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="loginHeadingOne">
		      <div class="panel-body login-body">
		        
		        <!-- Standard login -->

		        <div class="col-md-4"></div>
		        <div class="col-md-4">
		        	
		        	<form action="account-details.php" class="styled-form login-form">

				        <div class="form-group">
				          <label for="username">Login name</label>
				          <input type="text" class="form-control" id="username" placeholder="Username">
				        </div>

				        <div class="form-group">
				          <label for="password">Password</label>
				          <input type="password" class="form-control" id="password" placeholder="Password">
				        </div>

				        <div class="form-group forgot-username">
				        	<a href="#">Forgotten your username?</a>
				        </div>

				        <div class="form-group clearfix">
				        	<button type="submit" class="orange-fill btn-block login">Login</button>
				        </div>

						<div class="checkbox keep-me-loggedin">
				          <label>
				            <input type="checkbox" value="">
				            Keep me logged in
				          </label>
				        </div>

				      </form>

		        </div>
		        <div class="col-md-4"></div>

		      </div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="loginHeadingTwo">
		      <h4 class="panel-title">
		        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#loginAccordian" href="#loginCollapseTwo" aria-expanded="false" aria-controls="loginCollapseTwo">
		          New to Wilson &amp; Bradley?
		        </a>
		      </h4>
		    </div>
		    <div id="loginCollapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="loginHeadingTwo">
		      <div class="panel-body login-body">
		        
		        <!-- New registration -->

			        <div class="col-md-1"></div>
			        <div class="col-md-5 register-account">
			        	
			        	<h2>Non-Trade</h2>
			        	<p>You need to register first.</p>
			        	<a href="#" class="orange-fill">Register</a>
	
			        </div>
			        <div class="col-md-5 register-account">
			        	
								<h2 class="trade-header">Trade - Apply for a Trading account</h2>
			        	<p>If you do not already have an established Trading Account with us and would like to apply, please download the relevant Credit Application Form for your state from the Downloads section of our website. </p>
			        	<a href="#" class="orange-fill">Download</a>
	
			        </div>
			        <div class="col-md-1"></div>

		      </div>
		    </div>
		 </div>
		  <div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="loginHeadingThree">
		      <h4 class="panel-title">
		        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#loginAccordian" href="#loginCollapseThree" aria-expanded="false" aria-controls="loginCollapseThree">
		          Have an existing account? Request a web login
		        </a>
		      </h4>
		    </div>
		    <div id="loginCollapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="loginHeadingThree">
		      <div class="panel-body login-body request-account">
		        
		        <p>If you have a trading account with Wilson &amp; Bradley, you can apply for a web account online.</p>
		        <a href="#" class="orange-fill">Request</a>

		      </div>
		    </div>
		  </div>
		</div>

	</div>

</div>

<?php
  // Output footer and we're done!
  output_footer();
?>