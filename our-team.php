<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-sm hidden-xs">

			<h1>Sidebar Heading</h1>

			<ul>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li>
	              <a href="#" class="active">Sub Menu</a>
	              <ul>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#" class="active">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	              </ul>
	            </li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	        </ul>
	        
		</div>
		
		<div class="main-content">

			<h1 class="main-header">Our Team</h1>

			<div class="row">

                <?php for($i=0; $i<4; $i++) { ?>

                <div class="col-xs-12 col-md-6 our-team">
                    <div class="row">
                        <div class="col-xs-5 col-sm-3 col-md-5 team-photo">
                            <img class="img-responsive" src="img/our_team.png">
                        </div>
                        <div class="col-xs-7 col-sm-9 col-md-7">
                            <h2>Julia Stone</h2>
                            <h3>Manager</h3>
                            <p>
                                Expansion brought about the need to move and now after 30 years and three moves our head office
                                is located in Preston. preston is 15 minutes from the CBD and close to major arterial roads.
                            </p>
                        </div>
                    </div>
                </div>

                <?php } ?>

            </div>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>