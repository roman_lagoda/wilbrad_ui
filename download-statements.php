<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-sm hidden-xs">

			<?php output_sidebar('lists', 5); ?>
	        
		</div>
		
		<div class="main-content">

			<h1 class="main-header sub-header">DOWNLOAD STATEMENTS</h1>


				<table class="styled-table striped-table download-statement-table" cellspacing="0" width="100%">
					<tbody>
					  <tr><td><a href="#"><img src="img/icons/download_statement.png">January 2015</a></td></tr>
					  <tr><td><a href="#"><img src="img/icons/download_statement.png">February 2015</a></td></tr>
					  <tr><td><a href="#"><img src="img/icons/download_statement.png">March 2015</a></td></tr>
					  <tr><td><a href="#"><img src="img/icons/download_statement.png">April 2015</a></td></tr>
					  <tr><td><a href="#"><img src="img/icons/download_statement.png">May 2015</a></td></tr>
					  <tr><td><a href="#"><img src="img/icons/download_statement.png">June 2015</a></td></tr>
					  <tr><td><a href="#"><img src="img/icons/download_statement.png">July 2015</a></td></tr>
					  <tr><td><a href="#"><img src="img/icons/download_statement.png">August 2015</a></td></tr>
					  <tr><td><a href="#"><img src="img/icons/download_statement.png">September 2015</a></td></tr>
					  <tr><td><a href="#"><img src="img/icons/download_statement.png">October 2015</a></td></tr>
					  <tr><td><a href="#"><img src="img/icons/download_statement.png">November 2015</a></td></tr>
					  <tr><td><a href="#"><img src="img/icons/download_statement.png">December 2015</a></td></tr>
					</tbody>
				</table>

				<?php output_social_links(); ?>


		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>
