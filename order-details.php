<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container">
	<div class="sidebar-content-wrapper">

		<div class="main-sidebar hidden-sm hidden-xs">

			<h1>Sidebar Heading</h1>

			<ul>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li>
	              <a href="#" class="active">Sub Menu</a>
	              <ul>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#" class="active">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	                <li><a href="#">Sub Menu Link</a></li>
	              </ul>
	            </li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	            <li><a href="#">Sidebar Link</a></li>
	        </ul>
	        
		</div>
		
		<div class="main-content">

			<h1 class="main-header">Order Details</h1>

            <div class="order-details-info clearfix">
                
                <div class="clearfix hidden-xs product-list-header">
                    <div class="col-xs-6 col-sm-5ths col-md-5ths">Order Number</div>
                    <div class="col-xs-6 col-sm-5ths col-md-5ths">Date</div>
                    <div class="col-xs-6 col-sm-5ths col-md-5ths">Total Price</div>
                    <div class="col-xs-6 col-sm-5ths col-md-5ths">Add to Cart</div>
                    <div class="col-xs-6 col-sm-5ths col-md-5ths">Add to Favourite</div>
                </div>

                <div class="clearfix product-list-row">

                    <div class="col-xs-12 col-sm-5ths col-md-5ths">
                        <span class="visible-xs-block bold">Order Number</span>
                        W00115422
                    </div>
                    <div class="col-xs-6 col-sm-5ths col-md-5ths">
                        <span class="visible-xs-block bold">Date</span>
                        Mar 24, 2016
                    </div>
                    <div class="col-xs-6 col-sm-5ths col-md-5ths">
                        <span class="visible-xs-block bold">Total Price</span>
                        $200
                    </div>
                    <div class="col-xs-6 col-sm-5ths col-md-5ths">
                        <span class="visible-xs-block bold">Add to Cart</span>
                        <a href="#"><img src="img/icons/icon-nav-cart.png"></a>
                    </div>
                    <div class="col-xs-6 col-sm-5ths col-md-5ths">
                        <span class="visible-xs-block bold">Add to Favourites</span>
                        <a href="#"><img src="img/icons/favourite_star.png"></a>
                    </div>

                </div>

            </div>

            <h2>Product List</h2>
            
            <div class="hidden-xs clearfix product-list-header">
                <div class="col-sm-3 col-md-2">&nbsp;</div>
                <div class="col-sm-2 col-md-3">Product namekey</div>
                <div class="col-sm-4 col-md-4">Product name</div>
                <div class="col-sm-1 col-md-1">Qty</div>
                <div class="col-sm-2 col-md-2">Unit price</div>
            </div>

            <div class="visible-xs-block mobile-product-list-header"></div>

            <?php for($i=0; $i<4; $i++) { ?>

                <div class="clearfix product-list-row checkout-product-list-row">
                    <div class="col-xs-6 col-sm-3 col-md-2 product-image"><img src="img/home-grid/home-grid-all-spaces.jpg" class="img-responsive" /></div>
                    <div class="hidden-xs col-sm-2 col-md-3 product-namekey">MBKDK45</div>
                    <div class="col-xs-6 col-sm-4 col-md-4 product-name align-left">Blum METABOX Industrial Pack Of 100. Single Extension Steel Sides 118mm X 450mm. *Sides And Runners Only* 320K4500C15</div>
                    
                    <div class="hidden-xs col-sm-1 col-md-1 product-qty">99</div>
                    <div class="hidden-xs col-sm-2 col-md-2 product-price">$240.8828</div>
                    
                    <div class="col-xs-12 visible-xs-block price-display">
                        <div class="col-xs-4 col-sm-1 col-md-1 product-namekey">
                            <span>Namekey</span>
                            MBKDK45
                        </div>
                        <div class="col-xs-4 col-sm-2 col-md-2 product-qty">
                            <span>Qty</span>
                            99
                        </div>
                        <div class="col-xs-4 col-sm-2 col-md-1 product-price">
                            <span>Unit Price</span>
                            $240.8828
                        </div>
                    </div>
                    
                </div>

            <?php } ?>

            <?php output_social_links(); ?>

		</div>

	</div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>