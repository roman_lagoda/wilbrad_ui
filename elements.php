<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>
  
<div class="container">
  <div class="sidebar-content-wrapper">

    <div class="main-sidebar">

      <h1>Sidebar Heading</h1>

      <ul>
          <li><a href="#">Sidebar Link</a></li>
          <li><a href="#">Sidebar Link</a></li>
          <li><a href="#">Sidebar Link</a></li>
          <li><a href="#">Sidebar Link</a></li>
          <li>
            <a href="#" class="active">Sub Menu</a>
            <ul>
              <li><a href="#">Sub Menu Link</a></li>
              <li><a href="#">Sub Menu Link</a></li>
              <li><a href="#" class="active">Sub Menu Link</a></li>
              <li><a href="#">Sub Menu Link</a></li>
              <li><a href="#">Sub Menu Link</a></li>
              <li><a href="#">Sub Menu Link</a></li>
              <li><a href="#">Sub Menu Link</a></li>
            </ul>
          </li>
          <li><a href="#">Sidebar Link</a></li>
          <li><a href="#">Sidebar Link</a></li>
          <li><a href="#">Sidebar Link</a></li>
      </ul>

    </div>

    <div class="main-content">

      <!-- ####################################################### -->
      <!-- CONTENT                                                 -->
      <!-- ####################################################### -->

      <h1 class="dashed-header">Content</h1>

      <h1>Header H1</h1>
      <h2>Header H2</h2>
      <h3>Header H3</h3>
      <h4>Header H4</h4>
      <h5>Header H5</h5>

      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla elementum malesuada felis, eu bibendum tortor rhoncus sit amet. In hac habitasse platea dictumst. Mauris vestibulum interdum ante, eu gravida nibh bibendum ac. Sed molestie id tortor mollis aliquet. Fusce est elit, iaculis et aliquam a, iaculis eu diam. Sed pellentesque, turpis ut tristique pharetra, purus ligula imperdiet libero, eu varius dui sapien vel nunc. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed dapibus, sapien nec convallis rutrum, massa nibh placerat elit, quis sodales ligula tortor in lacus.</p>

      <p>Mauris fermentum fermentum magna, sit amet posuere lacus mattis ut. Donec facilisis, dui quis bibendum lacinia, metus nulla sollicitudin ex, non blandit sem lorem quis mauris. Suspendisse dictum suscipit justo, id aliquam odio ultrices ac. Nunc ut pretium tortor. Aliquam dolor augue, sollicitudin quis iaculis ut, venenatis ac quam. Etiam rutrum rhoncus tortor, ac sagittis nibh volutpat nec. Duis gravida semper dolor convallis scelerisque. Vestibulum in urna magna.</p>

      <!-- ####################################################### -->
      <!-- TABLES                                                  -->
      <!-- ####################################################### -->

      <h1 class="dashed-header">Tables</h1>

      <h2>Basic Info Table</h2>

      <div class="row">
        <div class="col-sm-6">

          <h3>Equal Width</h3>

          <table class="info-table equal-cols" cellspacing="0" width="100%">
            <tbody>
              <tr>
                <td>Info header</td>
                <td>Info data Info data Info data</td>
              </tr>
              <tr>
                <td>Info header</td>
                <td>Info data</td>
              </tr>
              <tr>
                <td>Info header</td>
                <td>Info data Info data Info data </td>
              </tr>
              <tr>
                <td>Info header</td>
                <td>Info data</td>
              </tr>
              <tr>
                <td>Info header</td>
                <td>Info data Info data Info data Info data </td>
              </tr>
              <tr>
                <td>Info header</td>
                <td>Info data Info data Info data </td>
              </tr>
              <tr>
                <td>Info header</td>
                <td>Info data Info data </td>
              </tr>
            </tbody>
          </table>

        </div>
        <div class="col-sm-6">

          <h3>Auto width</h3>

          <table class="info-table" cellspacing="0" width="100%">
            <tbody>
              <tr>
                <td>Info header</td>
                <td>Info data Info data Info data</td>
              </tr>
              <tr>
                <td>Info header</td>
                <td>Info data</td>
              </tr>
              <tr>
                <td>Info header</td>
                <td>Info data Info data Info data </td>
              </tr>
              <tr>
                <td>Info header</td>
                <td>Info data</td>
              </tr>
              <tr>
                <td>Info header</td>
                <td>Info data Info data Info data Info data </td>
              </tr>
              <tr>
                <td>Info header</td>
                <td>Info data Info data Info data </td>
              </tr>
              <tr>
                <td>Info header</td>
                <td>Info data Info data </td>
              </tr>
            </tbody>
          </table>

        </div>
      </div>

      <h2>DataTables</h2>

      <table class="dataTable" cellspacing="0" width="100%">
        <thead>
          <tr>
            <td>Heading</td>
            <td>Heading</td>
            <td>Heading</td>
            <td>Heading</td>
          </tr>
        </thead>

        <tbody>
          <?php for($i=0; $i<200; $i++) { ?>
            <tr>
              <td><?php echo uniqid('', true); ?></td>
              <td><?php echo uniqid('', true); ?></td>
              <td><?php echo uniqid('', true); ?></td>
              <td><?php echo uniqid('', true); ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>

      <h2>Product Listing Tables</h2>

      <div class="row">
        
        <div class="clearfix product-list-header">
          <div class="col-md-2">&nbsp;</div>
          <div class="col-md-2">Product namekey</div>
          <div class="col-md-4">Product name</div>
          <div class="col-md-2">Qty</div>
          <div class="col-md-2">Unit price</div>
        </div>

        <?php for($i=0; $i<5; $i++) { ?>

          <div class="clearfix product-list-row">

            <div class="col-md-2 product-image"><img src="img/home-grid/home-grid-all-spaces.jpg" class="img-responsive" /></div>
            <div class="col-md-2 product-namekey">MBKDK45</div>
            <div class="col-md-4 product-name">Blum METABOX Industrial Pack Of 100. Single Extension Steel Sides 118mm X 450mm. *Sides And Runners Only* 320K4500C15</div>
            <div class="col-md-2 product-qty">99</div>
            <div class="col-md-2 product-price">$10.50</div>

          </div>

        <?php } ?>

      </div>

      <!-- ####################################################### -->
      <!-- BUTTONS                                                 -->
      <!-- ####################################################### -->

      <h1 class="dashed-header">Buttons</h1>

      <h2>Normal</h2>

      <button class="orange-btn">A button</button>
      <button class="dark-btn">A button</button>
      <button class="orange-fill">A button</button>
      
      <a href="#" class="orange-btn">A link</a>
      <a href="#" class="dark-btn">A link</a>
      <a href="#" class="orange-fill">A link</a>

      <h2>Block</h2>

      <div class="row">
        <div class="col-sm-12" style="padding-top: 15px"><button class="orange-btn btn-block">A button</button></div>
        <div class="col-sm-12" style="padding-top: 15px"><button class="dark-btn btn-block">A button</button></div>
        <div class="col-sm-12" style="padding-top: 15px"><button class="orange-fill btn-block">A button</button></div>
        <div class="col-sm-4" style="padding-top: 15px"><a href="#" class="orange-btn btn-block">A link</a></div>
        <div class="col-sm-4" style="padding-top: 15px"><a href="#" class="dark-btn btn-block">A link</a></div>
        <div class="col-sm-4" style="padding-top: 15px"><a href="#" class="orange-fill btn-block">A link</a></div>
      </div>

      <!-- ####################################################### -->
      <!-- FORMS                                                   -->
      <!-- ####################################################### -->

      <h1 class="dashed-header">Forms</h1>

      <form class="styled-form">

        <div class="form-group">
          <label for="exampleInput1">Text input</label>
          <input type="text" class="form-control" id="exampleInput1" placeholder="Text Input">
          <p class="input-note">Note: We are unable to deliver to PO Boxes</p>
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
        </div>

        <div class="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>

        <div class="form-group">
          <label for="exampleInputTextarea1">Textarea</label>
          <textarea class="form-control" id="exampleInputTextarea1" rows="3"></textarea>
        </div>

        <!-- no special styling for now, just use browser default -->
        <!--
        <div class="form-group">
          <label for="exampleInputFile">File input</label>
          <input type="file" id="exampleInputFile">
          <p class="help-block">Example block-level help text here.</p>
        </div>
        -->

        <label>Checkbox Buttons</label>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="">
            Option one is this and that&mdash;be sure to include why it's great
          </label>
        </div>
        <div class="checkbox disabled">
          <label>
            <input type="checkbox" value="" disabled>
            Option two is disabled
          </label>
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="" checked>
            Option three is checked
          </label>
        </div>

        <label>Radio Buttons</label>
        <div class="radio">
          <label>
            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
            Option one is this and that&mdash;be sure to include why it's great
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
            Option two can be something else and selecting it will deselect option one
          </label>
        </div>
        <div class="radio disabled">
          <label>
            <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
            Option three is disabled
          </label>
        </div>

        <button type="submit" class="orange-btn btn-default">Submit</button>
      </form>

      <h1 class="dashed-header">Breadcrumbs</h1>

      <ol class="breadcrumb">
        <li><a href="#">Blum Products</a></li>
        <li><a href="#">Perfecting Motion</a></li>
        <li><a href="#">Blum AMBIA-LINE</a></li>
        <li><a href="#">AMBIA-LINE orion grey</a></li>
        <li class="active">AMBIA-LINE series 5 Blum AMBIA-LINE Steel Frame</li>
      </ol>

    </div>

  </div>
</div>

<?php
  // Output footer and we're done!
  output_footer();
?>