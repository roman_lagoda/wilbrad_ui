<?php
  // Grab template functions
  require_once('inc/template.php');

  // Output header
  output_header();

  // Home page content below:
?>

<div class="row header-feature-image hidden-xs" data-featureimg="http://<?php echo "$_SERVER[HTTP_HOST]"; ?>/wilbrad/img/feature-images/contact_feature_image.png">
	<img src="img/feature-images/contact_feature_image.png" class="img-responsive center-block">
</div>

<div class="container">

	<div class="main-content">
        <h1 class="main-header sub-header">Contact</h1>
        <div class="row">
            <div class="col-md-6 locations">
                <h2 class="main-header">Wilson &amp; Bradley Head Office</h2>

                <h3>Melbourne</h3>
                <p>
                    94 Bell St<br>
                    Preston, 3072<br>
                    Victoria, Australia<br>
                    ABN: 40211980801<br>
                    03 9495 8900<br>
                    <a href="mailto:sales@wilbrad.com.au">sales@wilbrad.com.au</a>
                </p>

                <h3 class="uppercase">State Representation</h3>
                <p>Our location details for other stores can be found in <a href="#">Locations</a></p>

                <h3 class="uppercase">Social Networks</h3>
                <div class="social-buttons">
                    <a href="#"><img src="img/icons/icon-footer-facebook.png"></a>
                    <a href="#"><img src="img/icons/icon-footer-twitter.png"></a>
                    <a href="#"><img src="img/icons/icon-footer-youtube.png"></a>
                </div>
                <p>
                    If you wish to get in contact with us for more information, enquires or feedback, please
                    complete the form and click on the "submit" button. One of our knowledgable staff members
                    will reply to your enquiry within 1 business day.<br>
                    If your enquiry is regarding a quote or to place an order, did you know you can view all your
                    pring and place order online 24/7 by logging in?<br>
                    To take advantage of our online ordering system simply register <a href="#">here</a> for account
                    customers and <a href="#">here</a> for retail customers.<br>
                    By logging in you you can also view your account information, order history and create favourite lists.
                </p>

            </div>
            <div class="col-md-6">

                <div class="main-contact-form">

                    <h2>SEND US A MESSAGE</h2>

                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="companyName" class="col-sm-4 control-label">* Company Name</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="companyName">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contactName" class="col-sm-4 control-label">* Contact Name</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="contactName">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contactPhone" class="col-sm-4 control-label">* Contact Phone</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="contactPhone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="contactEmail" class="col-sm-4 control-label">* Contact Email</label>
                            <div class="col-sm-8">
                            <input type="email" class="form-control" id="contactEmail">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="suburb" class="col-sm-4 control-label">* Suburb</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="suburb">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="state" class="col-sm-4 control-label">* State</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="state">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="regarding" class="col-sm-4 control-label">Regarding</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="regarding">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="yourMessage" class="col-sm-4 control-label">* Your Message</label>
                            <div class="col-sm-8">
                            <textarea class="form-control" id="yourMessage"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <p><small>Please enter the characters below as you see it</small></p>
                                <p>[CAPTCHA image here]</p>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="orange-btn">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
		</div>
	</div>

</div>

<?php
  // Output footer and we're done!
  output_footer();
?>